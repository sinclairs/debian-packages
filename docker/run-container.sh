#!/bin/bash

set -e
V=''

for D in $@; do
    ls -d ../$D
    [ -d ../$D ] || (echo $D not found && false)
    V="-v $PWD/../$D:$PWD/../$D $V"
done

xhost +local:root

echo
echo Warning: NOT running with --rm!  Remember to clean up!

#     -e DISPLAY --gpus all -v /tmp/.X11-unix:/tmp/.X11-unix --ipc host \
exec sudo docker run -v $PWD/../.git:$PWD/../.git $V \
     -v /dev/dri/card0:/dev/dri/card0 \
     -v $PWD/../results:/results \
     -it --cap-add=SYS_ADMIN --security-opt apparmor:unconfined debian-dev
