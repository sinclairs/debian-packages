#!/bin/sh

set -e

if ! [ -e ssh ]; then
    echo 'Warning: copying your ~/.ssh and ~/.gnupg to here and putting them in the Docker image!'
    sleep 5
    cp -vr ~/.ssh ssh
    tar -cjf gnupg.tar.bz2 -C ~ .gnupg
    cp -vr ~/.gbp.conf gbp.conf
    cp -vr ~/.pbuilderrc pbuilderrc
    cp -vr ~/.emacs emacs
    cp -vr ~/.emacs.d emacs.d
    cp -vr ~/.reportbugrc reportbugrc
fi

cat >devscripts <<EOF
DEBUILD_DPKG_BUILDPACKAGE_OPTS="-i -I -us -uc"
DEBUILD_LINTIAN_OPTS="-i -I --show-overrides"
DEBSIGN_KEYID="B6228EAB72F45172443554AF668D2B7D2C2963AB"
EOF

cat >dput.cf <<EOF
[mentors]
fqdn = mentors.debian.net
incoming = /upload
method = https
allow_unsigned_uploads = 0
progress_indicator = 2
EOF

cat >sbuildrc <<EOF
\$run_autopkgtest = 1;
\$autopkgtest_root_args = '';
\$autopkgtest_opts = [ '--', 'schroot', '%r-%a-sbuild' ];
EOF

docker build \
       --build-arg GROUPID=$(id -g) \
       --build-arg GROUP=$(id -ng) \
       --build-arg USERID=$(id -u) \
       --build-arg USER=$(id -nu) \
       -t debian-dev . || exit 1
