FROM debian:unstable

ARG USERID
ARG USER
ARG GROUPID
ARG GROUP

RUN apt update && apt install -y dpkg-dev build-essential \
  autopkgtest git-buildpackage dh-make quilt locales ccache emacs-nox \
  sbuild sudo dh-python python3-setuptools ssh

# Create with same UID and GID as system
RUN groupadd $GROUP -g $GROUPID \
&&  useradd -d /home/$USER -u $USERID -ms /bin/bash -g $GROUP -G sudo,users $USER \
&&  locale-gen en_US.UTF-8

COPY ssh /home/$USER/.ssh
COPY gnupg.tar.bz2 /home/$USER/gnupg.tar.bz2
COPY emacs /home/$USER/.emacs
COPY emacs.d /home/$USER/.emacs.d
COPY reportbugrc /home/$USER/.reportbugrc

RUN sbuild-adduser $USER

RUN chown -R $USERID:$GROUPID /home/$USER/.ssh
RUN chown -R $USERID:$GROUPID /home/$USER/.emacs
RUN chown -R $USERID:$GROUPID /home/$USER/.emacs.d
RUN tar -C /home/$USER -xf /home/$USER/gnupg.tar.bz2
RUN chown -R $USERID:$GROUPID /home/$USER/.gnupg
# RUN find /home/$USER/.gnupg -type f -exec chmod 700 {} \;
# RUN find /home/$USER/.gnupg -type d -exec chmod 600 {} \;

RUN sed --in-place -e 's/ALL=(ALL:ALL) ALL/ALL=(ALL) NOPASSWD: ALL/' /etc/sudoers

# RUN chown -R root:$GROUPID /var/cache/pbuilder
# RUN chmod -R 775 /var/cache/pbuilder
# RUN install -d -m 2775 -o $GROUPID -g $GROUPID /var/cache/pbuilder/ccache

# Temporary? https://ask.fedoraproject.org/t/sudo-setrlimit-rlimit-core-operation-not-permitted/4223
RUN echo "Set disable_coredump false" >>/etc/sudo.conf

USER $USERID
WORKDIR /home/$USER

RUN tar -xvjf gnupg.tar.bz2 && rm gnupg.tar.bz2

ENV LANG C.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL C.UTF-8
ENV DEBEMAIL="radarsat1@gmail.com"
ENV DEBFULLNAME="Stephen Sinclair"

RUN git config --global user.name "Stephen Sinclair" \
&&  git config --global user.email "radarsat1@gmail.com" \
&&  git config --global user.signingkey "B6228EAB72F45172443554AF668D2B7D2C2963AB"

COPY gbp.conf /home/$USER/.gbp.conf
COPY pbuilderrc /home/$USER/.pbuilderrc
COPY devscripts /home/$USER/.devscripts
COPY dput.cf /home/$USER/.dput.cf
COPY sbuildrc /home/$USER/.sbuildrc

RUN sudo chown $USERID:$GROUPID /home/$USER/.gbp.conf \
 && sudo chown $USERID:$GROUPID /home/$USER/.sbuildrc \
 && sudo chown $USERID:$GROUPID /home/$USER/.devscripts \
 && sudo chown $USERID:$GROUPID /home/$USER/.dput.cf \
 && echo source /usr/lib/git-core/git-sh-prompt >>/home/$USER/.bashrc \
 && echo USERCOLOR=35 >>/home/$USER/.bashrc \
 && echo DIRCOLOR=35 >>/home/$USER/.bashrc \
 && echo 'GNU_TTY=$(tty)' >>/home/$USER/.bashrc \
 && echo PS1='"\n${debian_chroot:+($debian_chroot)}\[\033[01;${USERCOLOR}m\]\u@\h\[\033[00m\]:\[\033[01;${DIRCOLOR}m\]\w\[\033[00m\]\[\033[01;34m\]\`__git_ps1 \" (%s)\"\`\[\033[00m\]\n\$ "' >>/home/$USER/.bashrc \
 && echo "EDITOR=emacs\nGIT_EDITOR=emacs\nUSER=\$(whoami)\nLANG=C" >>/home/$USER/.bashrc

RUN echo 'gbp buildpackage --git-builder=sbuild -A -v -s -d unstable' >>/home/$USER/.bash_history

RUN mkdir -p /home/sinclairs/projects/debian

RUN sudo sbuild-createchroot --include=eatmydata,ccache,gnupg \
  --make-sbuild-tarball=/srv/chroot/unstable-amd64-sbuild.tar.gz \
  unstable $(mktemp -d) http://130.89.149.21/debian

CMD /bin/bash
