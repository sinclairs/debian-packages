
# Process of adopting a package and getting used to Debian packaging

## Social factors (01/11/17)

There was a request for adoption (RFA) for Keras filed on the Debian
Science mailing list.  I am interested in Keras and it looked like a
fairly simple package so I thought it might be an easier way to learn
something than trying to do a package from scratch for Siconos.

https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=852133

First I emailed the RFA author and said I'm interested in adopting,
but how difficult is it, etc., and he said okay but that he doesn't
have time to walk me through it.  I responded on-list saying I might
have questions and requesting a mentor and they told me to just post
my questions to the list, so I did so.

Before I sent a public email though, I made sure I could create a git
repo with the package (using `gbp import-dsc`, see below) since the
maintainer said it is not versioned, and that I could build and
manipulate the package.  I updated the policy in a Docker and checked
lintian, etc., but deleted this temporary work.

Spent some time figuring out how to retitle the RFA bug to an ITA
(intent to adopt) -- just an email to the control list, but gmail
screwed up the formatting, adding a line-feed to the title (see
below), so I spent a whole afternoon getting 'mail' to work with the
gmail SMTP so that I could use lower-level email tools.

Next up is to submit an upload request (since I don't have dput
access) with a change in maintainership.  Normally it would be my name
but since it's a Science package I set it to Debian Science
Maintainers.  I make and push this commit and I will send a form email
today.

I might do the policy update (Debian Policy version 4.1.1) at the same
time, but I will update the package to latest Keras after that is
accepted.

## So many tools

There are many, many sets of tools to get to know.  Also, I am doing
this on Ubuntu so either a really good chroot is needed, or I do it in
Docker.  I'll try to summarize the tool sets and how they relate.

I was using Docker to run a clean sid environment for building but
today I'm checking out the usage of cowbuilder on Ubuntu to simulate
an image-based sid environment from `gbp buidpackage`:

Pbuilder: Build packages in a chroot environment --
https://wiki.ubuntu.com/PbuilderHowto
https://wiki.debian.org/git-pbuilder

Cowbuilder: Control Pbuilder using a copy-on-write method, so I set up
a chroot for sid using the commands,

    sudo apt-get install debian-archive-keyring
    sudo cowbuilder create --distribution sid \
        --mirror ftp://ftp.us.debian.org/debian/ \
        --debootstrapopts "--keyring=/usr/share/keyrings/debian-archive-keyring.gpg"

http://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.special.pbuilder.html

To use it with pbuilder, to build for sid while on Ubuntu, in the
package directory do:

    gbp buildpackage --git-pbuilder --git-dist=sid

http://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.special.pbuilder.html

I also enabled ccache for pbuilder with (1234 changed for my user/group id):

    sudo install -d -m 2775 -o 1234 -g 1234 /var/cache/pbuilder/ccache

and some edits to `.pbuilderrc` detailed at https://wiki.debian.org/git-pbuilder#Using_ccache

Now (later) I am setting up a Docker environment *in which* I want to
run cowbuilder/pbuilder.  That is, not using the Docker container for
the package's environment, but using it in place of my dev environment
so that I can install Debian tools without installing them globaly to
a server environment.  I found that after creating a clean Debian
Docker container, I could not successfully use cowbuilder as above,
because it requires access to /proc.  Work-around, don't put
everything in the Dockerfile, but do a run/commit step:

    docker build -t debian-dev .
    if docker run -it -privileged --cidfile pbuilderstep.cid debian-dev \
        sudo cowbuilder create (... rest of arguments from above)
    then
      docker commit `cat pbuilderstep.cid` debian-dev
      rm pbuilderstep.cid
    fi

This works, although the build fails at the initial clean step if
`python3-setuptools` is not installed in the container, this is now
added to the Dockerfile.

For final package testing, I set up autotestpkg with lxc, following
instructions at:

https://people.debian.org/~mpitt/autopkgtest/README.running-tests.html

Unfortunate that it doesn't support Docker but LXC is basically the
same thing as far as I can tell.  I created the initial container with,

    sudo autopkgtest-build-lxc debian sid

and then ran the test with,

    sudo autopkgtest ../keras_2.0.9-1.dsc ../python3-keras_2.0.9-1_all.deb -- lxc autopkgtest-sid

Works like a charm and discovered some bugs!  I tried using it with
the ssh option to a running Docker container, but no cigar.  The sudo
is very important, probably there are some lxc-related permissions I
need to set for my user.

I used `dlt` to check the `debian/copyright` file.  You can ask it to
automatically read the copyright info for a given file, so it's a nice
way to verify that you have specified it correctly.

http://pypi.debian.net/

## Tools configuration

I set `pbuilder = True` and `dist = sid` as the default in
`~/.gbp.conf`, did not add this to the package repository.  Not sure
if the `dist` part is needed, seems to default to sid without.

I found that pbuilder was not running lintian, so I added a hook in
$HOME/projects/debian/pbuilderhooks and installed it with HOOKDIR in
`~/.pbuilderrc`.  The lintian output is indeed in the output.

https://askubuntu.com/questions/140697/how-do-i-run-lintian-from-pbuilder-dist

I also added a hook to drop to a shell on error, otherwise inspection
of the cowbuilder chroot environment is painful:
https://wiki.debian.org/M68k/Cowbuilder

Experimenting with mounting /var/cache/pbuilder as tmpfs but not sure
it's worth it, let's see.  So far I've ended up unmounting it as I
*want* to cache the apt and ccache files between reboots!

I also tried adding a `BUILDUSERNAME=sinclairspbuilder` to
`.pbuilderrc` to enable a user and the use of fakeroot.  I had to
modify the cowbuilder environment and add a user with that name, and
it had to have the userid 1234 in `/etc/passwd/` to be able to write
to the cache.  I am not yet sure of the advantage of this.  I also
added `BUILDRESULTUID=1000` so that I could inspect build results
without sudo.  In any case it resulted annoying errors related to
userids and /etc/passwd right at the end of the build on running
lintian, so I disabled it for now.  Possibly I need to also get
`BUILDUSERID` correct, not sure if it should be my user
(sinclairs,1000) or a made up user for the chroot, or the fake user
with uid=1234.

I'm using git for managing the package source, so the "gbp" tool,
i.e. git-buildpackage is used to manage the repository.  I'm told
debian-science prefers the `--import-orig` method using release
tarballs, and that Python packages should use PyPI as the official
source, so this must be changed in the keras package.  Fortunately gbp
can call the rest of the debian tools for us, to build and run
lintian, sign the package, etc.

In the debian/gbp.conf committed to the package repository, I added
`pristine-tar = True` for `[import-orig]`.

## To adopt the bug, after configuring postfix and enable "less secure
## access" from my global google settings

Command to retitle the bug:

    echo "retitle 852133 ITA: keras -- deep learning framework running on Theano or TensorFlow" | mail -s "RFA: keras" control@bugs.debian.org

This sends the Intent to Adopt email to the debian control mail server.

Test first with self-mail because the gmail SMTP security is damn
stubborn.  Worked the first time but made a mistake, tried again
and it didn't work, tried again and then it worked.

Use `tail -f /var/log/mail.log` to verify in real-time in another
terminal window.

## Using reportbug

Go to google accounts settings, security, and allow "less secure
apps."  Then, run reportbug like this:

https://wiki.debian.org/reportbug#Using_GMail.27s_SMTP_Server

## Checklist before sending a package for review

* Check lintian output, fix everything.
* Check if standards should be updated.
* Check if dephelper level should be updated.
* Check if copyright year needs updating.
* C/C++ packages: re-generate symbol files and manage changes.
* Run all tests.
* Run autopkgtest.
* Check that `uscan --download-current-version` works.
* Run `cme check dpkg`, and `cme fix dpkg` if necessary, address all
  issues.

## Steps log (01/11/17)

I changed the maintainer line to Debian Science, added an Uploader
line with my name and email, added my name to `debian/copyright`, and
made a git commit.

I changed the section to python as recommended, and made a git commit.

I went through the Debian Policy update checklist and nothing really
applied except changing one URL in debian/copyright from http to
https.  I changed the policy to 4.1.1 and made a git commit.

I added pristine-tar to a gbp.conf configuration file that was
committed.

So there were four commits, and I used `gbp dch` to update the
changelog with the new version.  The changelog is marked UNRELEASED.
I changed the package version to 1.0.7-2 and my email, because there
were wrong. (It was marked as an Ubuntu package automatically, but
this is for sid.)

Currently testing this package with `gbp buildpackage` and then, if no
warnings from lintian, I will see how to send an appropriate email to
the list to get it uploaded.  Then I will be able to work on updating
Keras to the latest version.

The test is still running because this time for some reason it decided
to run all theano tests?  Anyways, I prepared and sent a sponsorship
request after adding "(Closes: #852133)" to a line in the changelog.

## Steps log (02/11/17)

After a request by the previous maintainer of keras to also take
lasagne, I checked the package and decided it was about the same
amount of work so I sent an ITA in response to his RFA by the same
manner, using `mail`.

This time there was a git repo in /git/collab-maint on git.debian.org
so I cloned it and made similar changes as with keras (maintainer,
policy, etc), but he saw my RFA and moved it to
/git/debian-science/packages.  I updated the URLs and pulled.  After
some testing I tried to push but there was a permissions error, so I
sshed in and saw that it still had the wrong group, which I reported
to him.  He created a new empty repo instead and I pushed the existing
lasagne repo to it.  Now waiting for him to accept and upload my HEAD
as a new upload, which I tagged using `gbp dch`.  I'll note that I had
to edit the output of `gbp dch` since it marked it as `1ubuntu-3`
instead of `-4`.  For keras he edited the changelog before uploading
so I'll wait and see what he does.

I also worked on the siconos package today, deciding that I needed to
re-do it after it was mentioned in response to one of my questions
that they prefer to work from tarball releases using import-orig.
However, it seems a bit more work is needed to make it compile, and
since I don't want to do it before Siconos 4.1.0 is out anyway, I
realized there were some hacks in the package that I had done that I
could probably integrate into Siconos before the release, to reduce
the work needed by `debian/rules` -- mainly this is setting the SONAME
and making `siconos_externals` static, but I think I've decided
against doing that.  I'll make `siconos_externals` an internal library
used by the `siconos_numerics` package instead.  I will add the
current package sources to the new siconos repository, erasing
previous history of changes to it, since they are useless anyways.

## Notes (03/11/17)

Need to package boost numeric_bindings as well:

https://github.com/uBLAS/numeric_bindings

The is a package already prepared but with out of date URLs at:

git://git.debian.org/debian-science/packages/boost-numeric-bindings.git

and a forgotten bug filed at:

https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=536270

Maybe useful also to look at Python-OCC:

git clone https://github.com/tpaviot/oce.git

git clone https://github.com/tpaviot/pythonocc-core.git

However oce.git may be ahead of the existing packages liboce-*.
It appears to be very important to synchronize the versions properly.

## Notes (05/11/17)

Daniel Stender (previous maintainer of keras/lasagne) uploaded the
lasagne package but made a few changes and had some comments:

- the repo misses the tags, they must be pushed or restored (the old
  repo is back in collab-maint).

- there were no upstream and pristine-tar branch, fixed (use "gbp
  clone" next time).

- the changelog is too sparse just repeating the commit messages and
  so, is must be stated exactly what has been changed, ideally sorted
  e.g. by the files in deb/ for optimal approach - I've rewritten it
  as a model what I mean. And there were things missing (adding
  yourself to deb/copyright).

- Vcs- fields in deb/control had to be updated.

So I forgot to push the tags and other branches! Oops.  Anyways, the
other things I certainly didn't know about, live and learn.

## Notes (06/11/17)

Now able to build the siconos packages and there is a slew of lintian
errors related to not closing an ITP bug, etc., but so far so good!
The main places to improve now are to generate symbol packages
properly (dev-pkg-without-shlib-symlink, no-symbols-control-file), and
to make things work with Python 3, but I need to wait for VTK-8 to be
uploaded for that, which seems to be in progress on the Debian Science
mailing list.

Spent a good deal of the day getting Siconos and Keras packages up to
scratch.  Updated Keras and got its tests running.  It seems there are
two test systems, `dh_auto_test`, and `autopkgtest`.  The former is to
run the actual unittests, while the latter is meant to quickly retest
the package when dependencies changes.  The tests were a bit stubborn
to get working since they refused to run when HOME was set to
`/nonexistent` as it is in the pbuilder envrionment.  I set it to
`/var/tmp` but I don't know if that's kosher.

I am currently adding a separate doc package to Keras, but building
the docs gave me two problems: firstly, I was confused why some files
in the github main directory were missing, in particular
contributing.md was needed to build the docs and it is missing.
Finally I realized these are not included in the PyPI dist.  Secondly,
I was able to build the docs outside pbuilder, but inside pbuilder I
am getting annoying Python 3 errors related to converting unicode to
ascii.  It seems that python3 reports the default filesystem encoding
as ascii, but I have no idea why, probably a missing locale setting in
the minimal pbuilder environment.  In any case I don't know how to fix
it except with a patch to add the encoding specification, so I'll ask
if that's the best way.  I am not sure I will finish this today, so
just noting here that that makes 2 or 3 questions for the mailing
list.

Update: Thought I was done but lintian reports I need a doc-base
control file, yet another thing to learn!

Questions:

1. Setting $HOME to /var/tmp, is it ok?
2. Copying project to /build/tmp and building the documentation there,
   or specifying PYTHONDONTWRITEBYTECODE=1, because otherwise it
   creates the `__pycache__` directories.  But actually in th elatter
   case one appears mysteriously anyways, and deleting it still
   results in a spurious error that I can't explain.
3. Shouldn't $BUILDDIR be /build? But this variable is empty, even if
   it seems to be set correctly in the interactive shell.

Last thing I am dealing with, lintian complains about
privacy-breach-generic due to some images linked to outside from the
HTML.  Not sure if the recommended thing to do is pull them in, but
when I try, I get errors about including unwanted binaries.  Seems
there is an included-binaries mechanism but I have to figure it out,
however I'm not sure if it's worth it so I'll ask the list.

## Notes (07/11/17)

Sent questions regarding pulling in CONTRIBUTORS.md as well as the
documentation images, whether that is appropriate, currently waiting
for a response.  Actually the question was really whether it's worth
switching upstream sources considering that CONTRIBUTORS.md gets
lost. Otherwise, I got documentation building properly finally.  It
turns out to work without setting HOME.  I left
PYTHONDONTWRITEBYTECODE=1 and will see if there are any comments.  I
fixed the URLs using patches, so now there are 3 patches.

Once a get a response about whether to switch upstreams, I will push
the master and hopefully get a more detailed review of the keras
package.

I added boost-numeric-bindings to the repository.  I found a more
up-to-date version (later changes) on github with an actual
"pre-release" from 2 years ago!  It actually has a Jamroot file, and
had 2 maintainers and a group name on github (uBLAS) so I think this
is the right upstream.  I changed debian/watch but it thinks it's an
older version due to comparing with the year.  I will force it, but
not sure if it will cause problems down the road, so I must ask the
question to the list later.

I tried to reopen the old RFP/ITP, but the system complained that it
was archived and could not be changed, so I used reportbug and filed a
new one.  (Manually type package "wnpp"!)
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=881109

I prepared the package completely for a sponsorship request but have
just noticed that this version on github no longer contains the
license file from the 2008 tarball.  I have filed two github issues,
one about running tests, and one about whether they could please add
the license file.  I hope it will not be a big problem.  I tried
checking the repository history but don't see the removal of the file,
so I guess it is based on an SVN that didn't include it in the first
place, which is rather unfortunate.  It does appear that a reference
to the license is in each header file, so maybe there is no problem.

I did get some tests running, but it requires adding a bunch of
dependencies so for now I am keeping this in a wip branch.  The thing
is, not all tests pass, so I am not sure what to do about that.

In any case, I pushed the branches (master,upstream,pristine-tar)
succesfully, and I was proceeding with asking for sponsorship, but
found that I need to upload the package to debian mentors and for that
I need to sign it.. ran out of time, gotta go.

I added `keyid = <mykeyid>` to my `~/.gbp.conf`, can be found using
`gpg --list-keys`, but that should only sign git tags.  Signed the
.dsc and .changes files using `debsign` as mentioned in PbuilderHowto.
Added relevant DEBSIGN environment variables to `~/.devscripts`.

I added mentors.debian.net info to `~/.dput.cf` as detailed in
https://mentors.debian.net/intro-maintainers

I uploaded using the command,

    dput mentors boost-numeric-bindings_0.99-1_amd64.changes

which worked, and I received a response in about 10 minutes with a URL
of my new package.  I then used reportbug again, saved my sponsorship
request in this directory for reference.

The relevant URLs are:

* https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=881118
* https://mentors.debian.net/package/boost-numeric-bindings

reportbug did not give me the chance to forward it to the Debian
Science mailing list, so I did this manually afterwards.  I hope they
tell me what the correct procedure is.

## Notes (09/11/17)

The numerics_bindings package encountered some scepticism on the
Debian Science mailing list.  It was mentioned that the package is
unmaintained, with links to the old maintainer.  I replied that that
is why I proposed a new upstream URL to the github of the uBLAS
maintainers.  So far no answer.  In the meantime my github issue
regarding the license file was responded to positively, but the one
about how to run the tests properly has not been.  Admittedly if the
tests are failing on e.g. BLAS, maybe it *is* unmaintained.

I spent a bunch of time yesterday playing with compiling Siconos with
all or pieces of externals disabled or moved into numerics, but it's
rough.  Perhaps they do have to be considered internal dependencies
after all, so maybe I'll just leave it.  It seems we depend on a
modifed, old version of SuiteSparse so currently we can't even use
that Debian package.  The rest are not packaged afaik.

Today I fixed up the final version of the Keras package and pushed it
and emailed Daniel to see if he can look at it and upload it.  Then I
put packaging aside and concentrated on some problems I found in
Siconos examples due to changes in the joints API.  Still trying to
simultaneously consider development and packaging so that it'll be
possible to propose a working package as soon as we release Siconos 4.1.

## Notes (10/11/17)

Trying to figure out what is necessary to get tests and examples
working for the Siconos package build.  When compiling with tests
enabled, due to RPATH being disabled the build fails while compiling
the test programs.  Currently evaluating how/whether to set
`LD_LIBRARY_PATH` or similar, can I put it as close to the compilation
stage as possible or does it need to be global?  Seems I can put it in
`auto_configure`, `auto_build`, checking if I can do it _only_ for
`auto_build` (no! .. neither work in fact), and whether I can even fix
it in the build system where it calls the tests to generate the cmake
files.  Even global `LD_LIBRARY_PATH` didn't work, miedo..  Finally:
Got tests working, had to add link to externals explicitly in each
components' CMakeLists.txt, as well as set `LD_LIBRARY_PATH` while
running ctest.

A separate build for running the tests could be possible but haven't
figured this out, even for python2/3 yet.  Currently I am thinking to
stick with Python 3 only, but that is contingent on VTK 8/9 getting
into Debian first, since vview would not be supported.  However, the
package will never be accepted if it is Python 2-only.

Meanwhile, a few examples are broken, notable BulletBouncingBox et
al., due to old ref files stuff that I never updated.  Updated the ref
files after a bit of testing and examining the results.

Also to do: Fix namespaces of public preprocessor definitions.  I
fixed it for HAVE_BULLET at least, to avoid problems in Gazebo, but it
should be done for the rest.

Fixed version number in docs too, made index.html a config file.

## Notes (14/11/17)

Debian package for Siconos builds, and now I am examining the package
dependencies by installing the packages individually and seeing what
gets installed, and whether I can run the examples, etc.

I've just discovered that I cannot compile via the `siconos` tool,
e.g. the examples in `examples/Numerics` without installing the rest
of the components, because the cmake scripts installed to
`/usr/share/siconos/cmake` expects all available compiled components
to be found.  So I have to change this strategy, making the `siconos`
tool depend on *everything*.  Not ideal, and it would be cool to fix
it upstream, but it seems like it would represent a lot of work.

## Notes (16/11/17)

Progress on the previous item, I found I could split up the generated
`siconosTargets.cmake` file, however I am not sure that it's worth it.
It's technically possible, but it means that the `-dev` packages are
tied in with the rather non-standard "siconos" tool that launches
CMake for the user.  I'm not really sure how well it would be
received, perhaps it's better to leave the "siconos" tool as its own
package that just depends on everything being installed.

The other thing that I discovered is that many numerics headers depend
publically on externals headers, which means I can't get away with
excluding them from the package.  This is rather bad news, since it
means that Siconos does nothing to hide these backend solvers, but
exposes and uses their data structures, which may be out of date
e.g. with regards to the already-packages SuiteSparse.  I may have to
create an externals package afterall and just see how well it's
received, or manually cut out as much *public* dependency on externals
as possible, but that will take some exploration.

I looked into this a bit, and it seems possible to remove most if not
all headers in externals from the installation, but with quite some
modifications.  I am not sure whether `SparseMatrix.h` should be
considered part of the API or not, since it piggy-backs off of things
in `csparse.h`, so that file would at least need to be included as
well.

Note: Maurice released Siconos 4.1.0 so I guess I will try to base the
package around that version for now, perhaps with patches for these
issues if necessary.  At least I'll work on that assumption, and if I
need to convince them to do 4.1.1 or 4.2.0 later before packaging will
work then that's okay.

On the Keras package, I got a response from Daniel the other day but
didn't have much time to look at it yet, I'll just paste verbatim some
of his comments for posterity:

    *) the images in debian/ are not covered by the license info in
    deb/copyright for debian/*, the paths could be just added to the
    other paragraph.

    *) The copyright span of upstream needs to be extended to 2017.

    > * calling the docs generator in a subshell, i.e., env
    >   LC_ALL=C.UTF-8 LANG=C.UTF-8 sh -c "generate docs command..";

    *) That's all right, good. I've came over exporting problems like
    this myself, if you've figured something out that just works
    that's fine.

    The doc-package is missing the html files. Maybe you could build
    the docs directly into
    debian/keras-doc/usr/share/doc/keras-doc/html ... then there's no
    need to put docs/html into debian/clean.

    *) you've chosen §DEB_BUILD_PROFILES, but being ready for build
    profiles would need other stuff like excluding build-deps when the
    package isn't tested, docs aren't build
    etc. (https://wiki.debian.org/BuildProfileSpec).  Maybe we stick
    to more basic §DEB_BUILD_OPTIONS (Policy 4.9.1) for now, keeping
    build profiles as an options for a later revision ...

    > To be honest I have no idea why this makes a difference.  Python
    > 3 (or mkdocs) must do something funky with the environment one
    > level up.  I also found that mkdocs does not respect
    > PYTHONDONTWRITEBYTECODE, no idea why or how.

    *) BTW: thank you Keras people for not using Sphinx like anybody
    else :-/

    *) On the separated examples, I would spare the examples package
    and ship them with the docs package (or ship them with the
    library), for I think a (rather small) examples packages like this
    is uncommon. If you have a model package you would like to follow
    we can keep that, though.

    However, the name should be "keras-examples" for they are not
    explicitly belonging to the python3 environment, that's common for
    supplementary packages like "keras-doc".  And use dh_install
    instead of dh_examples then, for the files belong into
    /usr/share/keras-examples/ (while the examples/README.md would
    belong into /usr/share/doc/keras-examples).

    *) you're going to need a debian/keras-doc.doc-base file (see in
    the doc-base package,
    /usr/share/doc/docs-base/doc-base.html/index.html).

    *) Great that the build time test are running, and the
    TESTTOIGNORE are written elegantly. Good job!

    *) The autopkgtests are missing something:
    <cut>
    autopkgtest [17:44:57]: test python3-theano-cpu: [-----------------------
    Using Theano backend.
    Traceback (most recent call last):
      File "<string>", line 1, in <module>
      File "/tmp/autopkgtest.hXlc68/build.AKQ/keras-2.0.9/keras/__init__.py", line 3, in <module>
        from . import utils
      File "/tmp/autopkgtest.hXlc68/build.AKQ/keras-2.0.9/keras/utils/__init__.py", line 6, in <module>
        from . import conv_utils
      File "/tmp/autopkgtest.hXlc68/build.AKQ/keras-2.0.9/keras/utils/conv_utils.py", line 3, in <module>
        from .. import backend as K
      File "/tmp/autopkgtest.hXlc68/build.AKQ/keras-2.0.9/keras/backend/__init__.py", line 80, in <module>
        from .theano_backend import *
      File "/tmp/autopkgtest.hXlc68/build.AKQ/keras-2.0.9/keras/backend/theano_backend.py", line 3, in <module>
        import theano
    ModuleNotFoundError: No module named 'theano'
    </cut>

    Ups, Theano isn't among the deps of the modules
    package. ${python3:Depends} doesn't catches all which is needed,
    so needed to be added manually, too (I've missed that).

    *) Vcs-Git and Vcs-Browser fields in deb/control.

    *) The changelog misses some changes like adding $KERAS_BACKEND,
    removing $LC_LANG from links, adding device to $THEANO_FLAGS,
    adding global export for $LC_ALL, add mkdocs to build-deps etc. I
    know this probably sucks, but we want to have Debian
    quality. Another model:

    <cut>
      * New upstream release:
        + update import-inspect-in-urllib-2.patch.
      * remove egg-info folder instead of ignoring it:
        + add M2Crypto.egg-info to debian/clean.
        + drop debian/source/options.
      * check tarball signature:
        + add debian/upstream with public key of <matej@ceplovi.cz>.
        + add pgpsigurlmangle to debian/watch.
      * handle SWIG/_m2crypto_wrap.c by diff-ignore in
        deb/source/options, drop it from deb/clean.
      * deb/clean:
        + add .cache/.
      * deb/control:
        + add python-typing to build-deps.
        + build against libssl-dev (Closes: #859225).
        + bump standards version to 4.1.1.
      * deb/copyright:
        + adjust some years according to LICENSE.
        + add Matěj Cepl to copyright holders.
      * deb/rules:
        + use PYBUILD_AFTER_BUILD to cp binding inplace for tests.
        + remove failsafe from tests.
        + shorten override for dh_auto_test, run tests from $(CURDIR).
      * tarball now ships with sphinx docs:
        + add override for dh_auto_install running sphinx-build in deb/rules.
        + drop deb/python-m2crypto.docs.
        + add python-sphinx to build-deps in deb/control.
        + add dh_sphinxdoc to dh extensions to run in deb/rules.
        + add ${sphinxdoc:Depends} for binary package in deb/control.
        + add prebuild doc/html to Files-Excluded in deb/copyright.
        + update existing doc-base files in deb/ for new folder.
        + add deb/python-m2crypto.doc-base.
      * add skip-test_urllib.patch.
    </cut>

    A helpful workflow would be to register every single change in the
    changelog immediately after it's done, and commit changes in the
    changelog together with the changes themselves.  And you are
    mixing present tense and past tense. I've used past, but now
    present which is shorter.

    *) When you've worked over the points you could toggle to
    "unstable" then, no need for me to touch the changelog, putting
    "team upload" etc.

I learned how to run autotestpkg, added to tools section above.  My
current command for running the whole shebang, that is, building the
package and testing it, is:

    gbp buildpackage -j3 --git-ignore-new && \
    sudo autopkgtest ../keras_2.0.9-1.dsc \
      ../python3-keras_2.0.9-1_all.deb -- lxc autopkgtest-sid

Actually I handled all cases above except fixing the copyright for
images and one thing left... it seems autopkgtest returns failure even
though my test succeeds!

## Notes (17/11/17)

Fixed the last issue from yesterday by adding `Restrictions:
allow-stderr` to the test control file `debian/tests/control`.  After
looking at the source it seems to be impossible to tell Keras to
suppress that output, so hopefully that flies with the sponsor.

Working on copyright issues for Keras, I learned about the
Files-Excluded field, which can actually be used to exclude files from
the downloaded tarball while it is packed for distribution.  Should
come in handy for Siconos!

I noticed a few more doc problems, such as inclusion of tracking
cookies, Travis build status that I think I should probably remove,
missing logo, etc.  The filenames don't correpond with what's on the
web, and now I see that there are already two new releases for Keras,
so before fixing these I will update the Keras tarball.  However,
trying to now change the upstream URL to PyPI since the missing file
has apparently been fixed due to my filing an issue, but it seems that
`pypi.debian.net` is down, so I will wait.

## Notes (08/12/17)

Regarding Keras, a while ago Daniel's only comments were some things
he wanted to improve in the changelog, so he made the changes and
uploaded.  Success!

He did agree with me that moving some javascript included in the docs
into its own packages (from the mkdocs package!) would be desirable,
but that this requires updates to dhinstalldoc and lintian.  Also
strange that lintian didn't detect the privacy issue with google
analytics.  He requested that I work on those but I sort of refused,
as I don't know Perl.  Who knows, maybe I can find some time to take a
shot at it.

As for Siconos, I spent the last 2 weeks fixing up some things I saw
as major problems with the way Siconos installs, that I could not
leave in the package.  Basically it installed all the headers, even
internal stuff and headers from external libraries.  While I haven't
been successful in moving use of externals into separate libraries, I
can at least try to make sure that these remain "internal details" of
the package -- if it's additionally exporting headers and symbols from
other people's software that strikes me as a major no-no.

Now I added the ability for Siconos to use system-installed
SuiteSparse, which allows me to replace the copy of CSparse in
externals with the copy of CXSparse and its header that come with the
libsuitesparse-dev package.  So now I am back to testing the package.

A couple of lessons learned just yesterday:

- It's better to give the .changes file to autopkgtest:

    sudo autopkgtest ../siconos_4.1.0-d13e392-1_amd64.changes  -- lxc autopkgtest-sid

- There exists a way to use Travis CI for package building and testing:
  http://travis.debian.net/

The latter I'll have to check out sometime, it seems to be basically
exactly what I'm doing on my local computer, but automated.

Right now with autopkgtest, I'm getting weird dependency errors
between the Siconos library packages that I don't understand:

    Correcting dependencies...Starting pkgProblemResolver with broken count: 2
    Starting 2 pkgProblemResolver with broken count: 2
    Investigating (0) libsiconos-io5:amd64 < none -> 4.1.0-d13e392-1 @un uN Ib >
    Broken libsiconos-io5:amd64 Depends on libsiconos-mechanics5:amd64 < none | 4.1.0-d13e392-1 @un uH > (= 4.1.0-d13e392-1)
      Considering libsiconos-mechanics5:amd64 0 as a solution to libsiconos-io5:amd64 0
      Holding Back libsiconos-io5:amd64 rather than change libsiconos-mechanics5:amd64

Finally solved it by doing a `cowbuilder --login` and manually trying
to install the packages, narrowing down the issue.  It turns out it
was because the name of the Bullet package changed, so
`libsiconos-mechanics5` refused to install.

Still some errors actually importing the python modules.  Oddly it
reports things like,

    ModuleNotFoundError: No module named '_numerics'

but clearly the module exists (although the packaging system renamed it..)

    # ls /usr/lib/python3/dist-packages/siconos/_numerics.*
    /usr/lib/python3/dist-packages/siconos/_numerics.cpython-36m-x86_64-linux-gnu.so

but maybe it's not a path problem, it seems there is a numpy dependency missing:

    ImportError: numpy.core.multiarray failed to import

indeed, I can manually import the modules after `apt-get install
python3-numpy`, so I'll add that to control.  Still an error with
`python3 -m siconos.numerics`, not sure what the difference is.

Okay, docs seem to say that the `-m` option is not meant to be used
with C extension modules.  Meanwhile, fixing the `python3-numpy`
dependency solved that problem.  Onto the next error... apparently,
classes missing in kernel:

    Traceback (most recent call last):
      File "<string>", line 1, in <module>
      File "/usr/lib/python3/dist-packages/siconos/mechanics/joints.py", line 3231, in <module>
        class NewtonEulerJointR(kernel.NewtonEulerR):
    AttributeError: module 'kernel' has no attribute 'NewtonEulerR'


## Notes (14/03/18)

Siconos changes are coming along and it is looking a lot cleaner, so
it will be time to work on the package again.  To prepared, I decided
it is time to update the Keras package.

I pulled in the new version, and changed things relatd to the new repo
url (moved to github team keras-team) and figured out which tests now
pass and fail, updated rules accordingly, ran all tests, and sent it
to Daniel for review.  I managed to miss some things.  So, now adding
a checklist at the top of this file.

I've been working on a Docker-based environment for building packages
which I am running on an Ubuntu server remotely.  I was able to build
and test Keras this way.  The container needs privilege in order to
mount things within the pbuilder/cowbuilder chroot.  Also the package
complains about a few build-depends that are not preinstalled in the
container, which I am not sure why that is necessary since I thought
pbuilder would install them, but it is basic stuff like the python2
part of dh tools and python setuptools needed for doing the initial
clean.  No big deal but surprising.

I had some weird errors with pbuilder for Siconos related "cowdancer",
but after trying and failing to update the cowbuilder image, I just
rebuilt the container and the problems went away.

Nothing in issue #32 in Siconos that I should make an fclib package
too.  I hope it's not too difficult to propose two packages at once.


## Notes (15/03/18)

Realized I should run autopkgtest from within the container as well.

Noticed in autopkgtest documentation that I should add `-e` flag to
lxc to tell it to use a temporary overlay instead of a full copy.

Therefore in the container I installed `autopkgtest` and `lxc`, then
did:

    sudo autopkgtest-build-lxc debian sid

This gave an error:

    ERROR: autopkgtest containers need networking; please set it up and adjust
    lxc.network.type in /etc/lxc/default.conf

The indicated file, `/etc/lxc/default.conf`, contained only the
string:

    lxc.network.type = empty

I changed it to,

    lxc.network.type = none

and then `autopkgtest-build-lxc` could proceed correctly.  This tells
nxc to use the host networking stack, like `--net=host` for Docker.

When running autopkgtest, I get some errors,

    lxc-create: storage/lvm.c: do_lvm_create: 122 No such file or directory - execlp
    lxc-create: storage/lvm.c: lvm_create: 421 Error creating new lvm blockdev /dev/lxc/autopkgtest-sid.new size 1073741824 bytes

but it seems to continue anyways.  However, I got errors related to
not finding `libeatmydata.so`, which is actually installed in the
Docker image but seems to be missing from the lxc image.

I found the following other utility for setting up the lxc image:
http://manpages.ubuntu.com/manpages/xenial/man1/adt-virt-lxd.1.html

which claims it will install eatmydata.  However, it is supposed to be
in the autopkgtest package but it is not available, so perhaps it is
Ubuntu-only.

Giving up for now and installing schroot as an alternative.  After
installing `schroot`, `sbuild`, and `ubuntu-dev-tools` packages,
following instructions at, https://wiki.debian.org/mk-sbuild I created
a chroot by running,

    mk-sbuild --debootstrap-include=devscripts unstable

it ends with an error,

    adduser: Only one or two names allowed.

It's because the USER variable was not set in Docker.

    export USER=`whoami`
    newgrp sbuild
    mk-sbuild --debootstrap-include=devscripts unstable

works.  So I'll add this to the Docker container.
This created a chroot called sid-amd64:

    $ schroot -l
    chroot:sid-amd64
    chroot:unstable-amd64
    chroot:unstable-amd64-source
    source:sid-amd64
    source:unstable-amd64

But,

    autopkgtest ../keras_2.1.5-1.dsc ../keras*.deb -- schroot sid-amd64

still fails.  It appears that running schroot also requires to mount
an overlay, which doesn't work inside Docker.

Instead I'll just use the chroot created by mk-sbuild:

    sudo autopkgtest ../keras_2.1.5-1.dsc ../python3-keras_2.1.5-1_all.deb -- chroot /var/lib/schroot/chroots/sid-amd64/

That worked and is actually fine because Docker provides the necessary
clean-up.  Of course autopkgtest can only be run once this way, so the
chroot would need to be re-built or re-copied each time.  Although,
instead of the schroot stuff, I can just use debootstrap directly to
create the chroot.  Or can't I just use the cowbuilder environment?

Yes, that appears to be working too,

    sudo cp -r /var/cache/pbuilder/cow.base ~/chroot
    sudo autopkgtest ../keras_2.1.5-1.dsc ../python3-keras_2.1.5-1_all.deb -- chroot ~/chroot

Works.  Although I don't know if it's as "minimal" as it's supposed to
be for running autopkgtest.

In any case the test, `python3-theano-cpu`, failed (expected) without
explanation (unexpected), and there were several errors mounting
devices --- maybe this is a fool's errand.

Trying directly with debootstrap instead of the use of mk-sbuild:

    sudo debootstrap --variant=buildd --include=devscripts sid ~/chroot
    sudo autopkgtest ../keras_2.1.5-1.dsc ../python3-keras_2.1.5-1_all.deb -- chroot ~/chroot

https://wiki.ubuntu.com/DebootstrapChroot

Yes, that works too, so sbuild/schroot etc are not needed in this
context since Docker provides a sufficiently assured "clean-up".

Of course, I can always just copy the dsc and deb files to my local
machine and run the working lxc container!  This is what I'm doing now
until this Docker-based setup is working.

On another topic, I found a very clear description of how to deal with
symbols files, which I will need soon:

https://aerostitch.github.io/linux_and_unix/debian/lintian_no-symbols-control-file.html


## Notes (16/03/18)

Got fclib updated and working with system suitesparse and the only
thing to do is make the symbols file.  Following the link above, I git
cloned a temporary copy of the repository in Docker (to not keep the
changes) and used `debuild` to directly build it locally, then
`dpkg-gensymbols` to generate the symbols, and `sed` to strip the
Debian version number.  I was confused why the symbols file has the
package version number in it though, rather than the ABI version
number, but it seems it's simply to track which version of the package
first introduced a certain symbol.  It's up to the maintainer to
synchronize this with ABI changes (using help from `dpkg-gensymbols`
to find diffs.)

https://wiki.ubuntu.com/stefanlsd/dpkg-gensymbols

Anyways, added a pkg-config file to fclib, added a basic read/write
test for fclib info for autopkgtest, and finished the package for now,
I hope.  I will propose to debian-science soon, after tagging 3.0.0
and changing the version numbers accordingly.  I'll rebuild the git
from scratch for the fresh release, as it's quite messy now.

Nope, I was too quick to say that.  Generated the package for fclib
and autopkgtest gives me errors,

    badpkg: Test dependencies are unsatisfiable. ...

It may be because I used `@` in the Depends list in
`debian/tests/control`.

## Notes (17/03/18)

Attempting to fix that last problem with autopkgtest, I changed the
`@` for `libfclib-dev` and it passed that dependencies issue, but it
couldn't successfully compile and run the test, at first because I
forgot to add the `.pc` file to the dev package, and then because I
needed to add the `libfclib.so -> libfclib.so.0` symlink also to the
dev package, but then it *still* would not compile saying that `fclib`
cannot be found.  It appeared that the library path
`-L/usr/lib/x86_64-linux-gnu` was not being added by `pkg-config`,
which I spent some time investigating, but I conclude that this is
probably correct behaviour because it is a standard path.  Now
checking if it's because I don't have a correct dependency on
`libfclib0`.

I added `libfclib0` to the test dependencies to check, and back comes
that "unsatisfiable" error, maybe that explains that, but I don't see
why it can't satisfy that package as I am providing it.

Indeed, I forgot that `libfclib-dev` does *not* have a dependency on
`libfclib0`, and I am not sure if it should, so I need to check that.
Later: I added it but it generated `weak-library-dev-dependency`.
Needed to add `(= ${binary:Version})`.

In the meantime, I destroyed and am creating the lxc container from
scratch using `autopkgtest-build-lxc` because I realized that perhaps
part of the error message that I did not quote above, saying to update
it, should not be ignored, since I created the lxc container quite
some time ago, and maybe dependencies can't be satisfied because of a
dependency of a dependency.

Result: no.  Updating the container did not help, dependencies still
not satisfiable.

Ok, checking higher in the error messages I finally found some
information:

    Correcting dependencies...Starting pkgProblemResolver with broken count: 1
    Starting 2 pkgProblemResolver with broken count: 1
    Investigating (0) autopkgtest-satdep:amd64 < 0 @iU mK Nb Ib >
    Broken autopkgtest-satdep:amd64 Depends on libfclib0:amd64 < none | 2.0.7-1 @un uH >
      Considering libfclib0:amd64 1 as a solution to autopkgtest-satdep:amd64 -2
      Removing autopkgtest-satdep:amd64 rather than change libfclib0:amd64
    Done
     Done
    Starting pkgProblemResolver with broken count: 0
    Starting 2 pkgProblemResolver with broken count: 0
    Done

I should have gone straight away to the autopkgtest documentation:
https://people.debian.org/~mpitt/autopkgtest/README.package-tests.html
But I don't see anything there that explains the problem unfortunately.

An example of running unit tests from autopkgtest:
https://bazaar.launchpad.net/+branch/ubiquity/view/head:/debian/tests/unit

    #! /bin/sh
    exec "${0%/*}/../../tests/run" --installed

Discovered I can get into a shell in the autopkgtest environment by
passing the `-s` option, and trying to manually install my packages
from that shell revealed that my dependency on `libhdf5` was wrong,
because the package is actually named `libhdf5-100`.  It's unfortunate
that there seemed to be no error messages indicating that I had a
dependency on a non-existent package.  With that change I was able to
move forward, fix a missing dependency on `pkg-config` in the tests,
and got `autopkgtest` to pass!

So, after `build-container.sh` has been successfully run, the full
commands for building the package and running autopkgtest (on laptop)
are:

    ./run-container.sh "cd projects/debian/fclib; gbp buildpackage --git-ignore-new"
    sudo autopkgtest ../*fclib*.{changes,deb} -- lxc autopkgtest-sid


## Notes (20/03/18)

I ironed out the last problems with fclib, which indeed need a few
upstream changes (e.g. making the headers nicer when installed, etc.)
Now re-recreating the git repo from scratch and figuring out how to
create a project under science-team on salsa.

Finally I am not sure whether to create a personal project on salsa or
to request to create one under science-team, so I sent an email to the
list to ask.  They got back to me within 5 minutes with a new project
"fclib" created under science-team.  I promptly asked for them to do
the same for "siconos", which was also done quite quickly.  Now just
waiting to finalize the fclib release and I'll propose the package.

## Notes (23/03/18)

Just realized a couple of things I've been doing wrong.  Daniel said
that my uploads did not contain the upstream tarballs, and I could not
understand why, until I just slapped my forehead and realized I've
been doing "git push origin master" instead of "git push origin
--all".  Important, since 3 branches must be synced!

Aside from that, I saw that my changes can be verified via
build/autopkgtest on the Debian CI server, here:
https://ci.debian.net/

e.g., keras progress:
https://ci.debian.net/packages/k/keras/unstable/amd64/

It seems that lasagne is not listed though, perhaps because my new
package containing autopkgtest tests has not been accepted yet?

Today I solved the last of the failing build-time tests for Siconos,
so now it's just to fix lintian stuff.  Down to manpage errors and
missing symbols files.

## Notes (24/03/18)

So I have received emails from both Daniel Stender and Rebecca Palmer
saying that my lasagne package either doesn't build, doesn't run
tests, or fails the autopkgtest for Python 3.  And keras doesn't build
because it was missing a dependency on python3-distutils, which I can
see in the ci.debian.org build logs.  This has left me confused
because these packages build and test fine on my setup.  Confused to
the point that I simply sent a blank email to both of them describing
the steps I use to see if they can point out any mistakes I am making.
I am waiting for an answer.  In the meantime I am trying now the steps
that Daniel says he uses for autopkgtest which is with schroot instead
of lxc:

    A procedure that I found isn't bad is to use Sbuild chroot
    tarballs together with autopkgtest, that runs locally against the
    .changes file after the package has been build (BTW, lasagne did)
    like:
    
    $ autopkgtest lasagne_0.1+git20180322.37ca134-1_amd64.changes -- schroot unstable-amd64-sbuild
    
    Keeping it updated is from the Sbuild toolchain:
    $ sudo sbuild-update -udcar unstable-amd64-sbuild
    
    Creating the tarball runs like:
    $ sudo sbuild-createchroot --make-sbuild-tarball=/var/lib/sbuild/unstable-amd64.tar.gz unstable `mktemp -d` http://httpredir.debian.org/debian

    Sbuild isn't bad at all, worth taking a look (if haven't
    already). I haven't figured out how to run reproducible test
    builds (consecutively with two totally different environments)
    with it yet, if you going to have something on that in the future
    please share.

Also installing the tool gives the command,

    sbuild -d unstable keras_2.1.5-2

However this complains,

    E: apt-cache returned no information about keras source
    E: Are there any deb-src lines in your /etc/apt/sources.list?

Regardless, I rebuilt the package using `gbp buildpackage` and ran
autopkgtest using schroot as above, for both lasagne and keras, and
got no errors, so I am no closer to understanding why the others are
not getting the same results.

## Notes (25/03/18)

More documentation on how to use sbuild,

https://wiki.debian.org/sbuild

And I can build a package with debs found outside the archive with,

    sbuild --extra-package=./foo.deb

So I used theano debs that I built from salsa master with the
following command,

    sbuild --extra-package=../python3-theano_1.0.1+dfsg-1_amd64.deb  --extra-package=../python-theano_1.0.1+dfsg-1_amd64.deb | tee ../lasagne-sbuild.log

but I got the same results as with cowbuilder/lxc: all tests run and
everything passes.  I will send the build logs to Daniel and Rebecca
when I get the chance, I guess.  I wish Lasagne would show up on the
CI server, but I think it's because previously it did not have
autopkgtests.

## Notes (26/03/18)

It appears that lasagne has been uploaded and now shows up on
ci.debian.org!  Unfortunately no explanation from Rebecca or Daniel,
but I emailed to let them know that I saw it and gave a thanks.

Now, filed an ITP for fclib:
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=894120

Pushed to salsa, and signed and uploaded the packages via FTP. (The
HTTPS upload gave a 302 error..)  Received the email from
mentors.debian.net: https://mentors.debian.net/package/fclib and
sponsorship request bug
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=894128

Sent the mentors sponsorship request, and forwarded to debian science
mailing list.  Waiting for a reply.

Just discovered I have an overview page for my packages:
https://qa.debian.org/developer.php?login=radarsat1%40gmail.com

## Notes (28/03/18)

My boost-numeric-bindings RFS bug got closed last night due to no
sponsorship for too long.  Fair enough, it seems many of the tests
lead to errors, so maybe it just needs work.  In the meantime it is
header-only and gets compiled into Siconos, so no big deal I guess.
In the meantime I updated URLs to salsa and updated package version
and compat and such.

## Notes (02/04/18)

Over the weekend got a review on my fclib package.

    Hi Stephen,
    
    your package looks very good! A couple of notes:
    
    1) Please clarify and add in d/copyright an information about
    license of CSparse files. It is extremely important.
    2) I am not able to download the tarball using watch-file:
    uscan --force-download
    
    Could not read ..//FrictionalContactLibrary/fclib/archive/v3.0.0.tar.gz:
    No such file or directory at /usr/bin/mk-origtargz line 398.
    uscan: error: mk-origtargz --package fclib --version 3.0.0
    --compression gzip --directory .. --copyright-file debian/copyright
    ..//FrictionalContactLibrary/fclib/archive/v3.0.0.tar.gz subprocess
    returned exit status 2
    
    3) Apply cme fix dpkg
    
    Please fix those issues and I will upload the package.
    
    Regards
    
    Anton

I've already fixed the copyright issue but will also offer to just
remove the CSparse files since they are not actually used in the
build.  Working on the second one.  For the third one, it turns out
there is a whole tool I didn't know about (more tools!) called cme:

https://github.com/dod38fr/config-model/wiki/Managing-Debian-packages-with-cme

For debugging uscan watch files:

    uscan --verbose --report

Strange situation: the `cme` command removed version of gcc in
build-depends,

    Warning in 'control source Build-Depends:2' value 'gcc (>= 6.0)': unnecessary greater-than versioned dependency: gcc (>= 6.0).

but this led to a lintian error telling me to remove any
build-essential packages from build-depends,

    E: fclib source: build-depends-on-build-essential-package-without-using-version gcc [build-depends: gcc]

so I removed gcc entirely, but this led to the error in pbuilder,

    dpkg-checkbuilddeps: error: Unmet build dependencies: build-essential:native

But when I add build-essential I get lintian error,

    build-depends-on-build-essential

Okay, after a lot of googling I finally convinced myself that it is
correct not to list build-essential or gcc, and the pbuilder
environment is supposed to come with it preinstlaled, so it must be
missing.  Therefore I logged into the cowbuilder environment
(`cowbuilder --login`) and indeed gcc was available.  Then I ran `gbp
buildpackage` and noticed that it was *uninstalling* build-essential
at the beginning of the run, due to trying to install `gcc-8-base`.
So maybe an issue with using an old image, so I am upgrading the
cowbuilder environment to test this.  Unfortunately it appears I
cannot upgrade it in the Docker container due to the remaining
problems with "Invalid cross-device link" (unsurprising), so I will
rebuild the container, which takes some time.

After: yes, that seems to have fixed it.  Damn, what a waste of many
hours.  Now pushing the changes and answering the email on each point.

## Notes (05/04/2018)

Yesterday and today I have been debugging issues with the Siconos docs
and examples packages.  I just wanted to note that I got fed up with
how long `gbp buidpackage` takes and how difficult it is to inspect
problems, and it occurred to me that inside the Docker environment,
which is running sid, it is harmless to install packages and do things
manually.  So in the container, I have installed the build depends of
Siconos and have been running `dpkg-buildpackage` manually, and
running `lintian` manually to check the results.  This also allowed me
to install `ccache` and re-run things without cleaning, etc., and even
run specific `dh_*` commands in verbose mode, which has been super
helpful.

A couple of environment variables I set:

    export DEB_BUILD_OPTIONS=nocheck
    export PATH=/usr/lib/ccache:$PATH

Of course I will still check everything in a clean `pbuilder`
environment using `gbp` as before when I am done debugging things.

## Notes (10/04/2018)

Things looking up, the package is down to symbols files errors, adding
autopkgtest, and some decisions about what to include.  So I am now
looking at how to use `dpkg-gensymbols` with `c++filt`.

Oh, apparently it's as easy as piping the output to `c++filt`.
Hopefully this diary entry is very short then.

## Notes (11/04/2018)

Follow-up from yesterday.  I tried leaving out "externals" which is in
the numerics package, from the symbols file, but I got the error:

    dpkg-gensymbols: warning: new libraries appeared in the symbols file: libsiconos_externals.so.5
    dpkg-gensymbols: warning: debian/libsiconos-numerics5/DEBIAN/symbols doesn't match completely debian/libsiconos-numerics5.symbols

Well, I didn't want to maintain these symbols are part of the package
interface because they it be considered an "internal library", but it
seems Debian policy doesn't apply to internal shared libraries and
that case is not well documented.

I'm not quite sure how/if to quiet this error so I added the externals
symbols to the numerics package symbols file to see if it fixes the
problem.

And of coure it was not as simple as what I wrote yesterday, since I
am getting errors regarding the strings output from `c++filt` that are
not recognized when `dpkg-gensymbols` checks the package during
buildpackage.

I am looking into the use of these tools to help with C++:

http://pkg-kde.alioth.debian.org/symbolfiles.html

Those didn't help, but ok, I figured it out.  The C++ symbols need to
be surrounded by quotes and prepended by `(c++)`.  The non-mangled
symbols need to be left alone!  Surprising there are no tools to
really do this, it seems it has to be done by hand.

Found [this tidbit](https://stackoverflow.com/questions/12664207/how-to-adjust-symbols-file-in-a-debian-package-for-a-shared-lib):

> You might also want to know that you can
> `export DPKG_GENSYMBOLS_CHECK_LEVEL=0` and
> in some cases not worry about the problem at all.

In any case, now that all the symbols are correctly marked as
translated from C++ mangled names, and the C functions are unmarked,
it seems to pass the gensymbols stage fo the build!

## Notes (12/04/2018)

After bugging the person who reviewed my fclib package, I got the
response saying he would upload it but to remove the unneeded sources
and add `+dfsg`.  I'm not sure this is necessary as those files do not
offend the DFSG, but I'm not one to argue ;)

> I am ready to upload the package. But I would propose
> you to remove not used 3rd-party source, adding them
> into the d/copyright under the field "Flies-Excluded".
>
> Also please do not forget to add +dfsg prefix to the upstream
> version.
>
> "uscan --download --repack"  will create the stripped sources.

Trying that uscan trick now.  I wasn't sure from reading on the web
weather uscan treated Files-Excluded properly.  Trying to figure out
how to get `gbp` to use uscan that way.  It won't.  I used uscan and
then `gbp import-orig`'d manually, adding `+dfsg` to the version
string.

However, I got a weird git error due to using submodules and had to do
this in a `gbp clone` of the repository.

    Unable to create '/home/sinclairs/projects/debian/fclib/.git/gbp_index.lock': Not a directory

Odd.  But it worked, and I just pulled the changes into my usual place
without issues (albeit once for each branch, `gbp pull` is a bit
awkward.)

## Notes (19/04/2018)

In some email exchanges on the mailing list I saw a reference to
buildd CI infrastructure.  I don't know if I've mentioned it in these
notes and I have no idea what the relation is with
https://ci.debian.net/ but I'll put the link here regardless.

https://buildd.debian.org/status/package.php?p=fclib

I get the impression that ci.debian.net is more for autopkgtest,
whereas buildd is the server used for actually building Debian
packages on all architectures.

Still waiting for fclib to go from the "NEW" upload queue into sid,
however that works.  It seems it requires manual intervention and
there are a bunch of node.js packages ahead of me.

## Notes (30/05/2018)

Back from vacation and my fclib package was rejected because a couple
of Latex files (the INRIA template files) refer to a different
license, the LPPL (latex license).  So, I added it verbatim (it's
long) and ask if this is okay to be updated just like that.

Some bug was reported on Keras not finding its default Tensorflow
backend, need to maybe provide a patch for it to default to Theano or
at least be quiet, but maybe that's for tomorrow.

## Notes (12/06/2018)

Have been updating keras.  There was a bug filed against the package
regarding the backend default, but in fixing that I found that there
were a new release (2.2.0) so I updated only to find that the upstream
has split off some keras modules to their own packages.  I requested
and received the space on salsa for each one and made packages.  There
are still some issues to iron out but basically everything is working,
although I am blocked a bit on a bug (broken symlink) in the mkdocs
package.  Here's the email I sent to Daniel:

> The new keras packages are ready except for three issues:
>
> (1) The keras-docs package fails to build due to a bug in the mkdocs package:
> https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=901318
>
> (2) I wanted python3-keras-preprocessing and
> python3-keras-applications to be simple dependencies of python3-keras,
> however dh_python3 causes automatic dependencies to be found on keras.
> This leads to a circular dependency.  Is this problematic?  It means
> that `python3-keras` gets installed during build of these packages,
> even though the dependency goes the other way, which is weird.  I can
> fix it by patching setup.py in each package to remove keras from
> install_requires, but I am not sure if it's necessary, since build
> does succeed.
>
> (3) The distribution tarball for 2.2.0 (from pypi) appears to contain
> some rogue .pyc files.  Should I eliminate these by a patch or repack?
>  They don't actually cause any build issues, but are annoying,
>
> Regarding (1), you can build the package if you specify DEB_BUILD_OPTIONS=nodoc.

Anyways, hopefully this can all be uploaded fairly quickly, although
the mkdocs bug is rather annoying.

## Notes (18/10/2018)

As usual I cannot keep a journal properly.  Haven't written in months,
despite having been working on the Siconos package again.

## Notes (06/4/2019)

Unbelievably I am still trying to get siconos uploaded, however I seem
to now have a dialog with Jonathan Carter on `mentors.debian.net`.

Anyways, two notes:

1. I was blocked from re-uploading because I had repacked the tarball.
   It appears you can track tarball version of the same release by
   appending a point-release to dsfg, i.e. I am now using
   4.2.0+git20181026.0ee5349+dfsg.2-1 as the version number.

2. It has been annoying to sign the package after `gbp buildpackage`
   because it asks for my passphrase.  I found the following works to
   enter the passphrase in advance, but keep in mind that it's not
   very secure:

    gbp buildpackage --force-sign \
      -p'gpg --pinentry-mode loopback --passphrase <passphrase> --batch --yes' \
      --force-sign

## Notes (13/9/2019)

Not noted in last entry is that the siconos package was finally
uploaded by Jonathan Carter as 4.2.0+git20181026.0ee5349+dfsg.2-1.  It
sat in the NEW queue for several months and finally just a few days
ago it was approved!  You can now apt-get install it.  Of course now I
am checking it and I see that it fails in ci.debian.net on
autopkgtest.  It appears that I left a mistake in the sed command that
I use to replace the compiler in the installed CMakeLists.txt, so that
the "siconos" script uses a non-existent compiler.  I know how to fix
it but I will likely have to generate a new package and go through the
mentors process again. Fortunately it is no longer a NEW package, so
now I'll get to find out how long updating a package takes.  I would
also like to understand why I did not encounter this while running
autopkgtest locally under schroot.  For now though, I will file a bug
via reportbug so that I have something to refer to while submitting a
fix.

## Notes (02/2/2020)

Getting back into updating the siconos package as I have more time now
and it is long overdue.  I was able to fix the problem with the
CMakeLists.txt file, and also found that two test were failing, which
mysteriously did not fail previously.  One was really a bug: a wrong
ref file, the other is strange as the ref file only partially matches,
so I don't know what would have changed.  In any case, I committed
some fixes for these and am ready to upload a new package to mentors,
but have gotten stuck on running autopkgtest due to being unable to
run debootstrap correctly, of all things.  I keep getting an error of
a perl-related package dependency which seems broken, the only
reference to it a bug filed against debootstrap that is reassigned to
another bug without further comment.

https://www.mail-archive.com/debian-bugs-dist@lists.debian.org/msg1688537.html

Finally after thinking about it, I realized that since the bug is
about a package dependency on an non-existent package
perl-openssl-abi-1.1, I tried specifying manually the replacement
package and that seems to have done the trick:

    mk-sbuild --debootstrap-include devscripts,dh-python,perl-openssl-defaults unstable

Not obvious at all, this kind of thing that stops me making progress
all day really makes Debian maintenance distasteful, if I am to be
completely honest.  It would have been nice if someone had posted the
work-around at least.  (The re-assigned bug is dated 2016!)

Anyways, now I can proceed.  However, it seems my chroot is not
working and autopkgtest fails with errors like,

    autopkgtest [19:51:40]: test kernel-dev: [-----------------------
    bash: /dev/fd/62: No such file or directory

That was in Docker using a chroot copy however, now I am trying
outside Docker using schroot.  After fixing some issues with how
I previously nuked my schroots, autopkgtest now passing.

## Notes (03/2/2020)

Uploaded last night, but found that some lintian warnings showed up on
mentors, so I am rebuilding now.  Discovered my Docker image was way
out of date and ran into some errors doing a `dist-upgrade` (oddly
enough, specifically with the lintian package), so I cleaned all the
images off the computer and rebuilt debian-dev from scratch using the
`build-container.sh` script, and it worked beautifully, very
satisfying.

Update: except apparently gpg signing did not work.  I had to
re-import my key manually into the container, using `gpg
--export-secret-key` and `gpg --import`.  Not sure why, but I need to
integrate that into the image so I'll work on it when I have time.

Anyways, package signed and uploaded again, and now I have filed a
sponsorship-request.. found a nice reminder of how to do things here:

    http://arnaudr.io/2016/10/01/publishing-a-debian-package-mentors-sponsorship/

I did have a question regarding whether the package should be marked
UNRELEASED or unstable.. these little details always seem to go
undocumented.  So I had the idea to stop by IRC channel #debian-menors
on irc.debian.org and was told it should be UNRELEASED.  I already
uploaded it as unstable so I'll leave it for now, hopefully no changes
necessary.  (mentors reports lintian clean.)  But I'll remember to ask
questions in IRC again the future, much better to get some immediate
feedback like that.

Also in IRC they mentioned the "dgit" tool, I asked how they stay
informed of new tools and suggested I follow the debian-devel-accounce
mailing list, which I now signed up to.

It's been a short while and I haven't yet received any confirmation of
my sponsorship request.

## Notes (13/2/2020)

So I had been watching the page on mentors on and off to no avail, and
today I remembered that I had not received a confirmation of my
sponsorship request bug.  Indeed, after checking, it was not filed!
I realized it's because I didn't set up reportbug properly in Docker,
and in fact it told me it "sent" the bug successfully, but probably
only "sent" it locally to the Docker container.. will have to watch
for that in the future.

This time I was sure to configure the gmail smtp correctly and almost
immediately received confirmation:

    https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=951254

Next time I'll be more careful.

## Notes (16/2/2020)

The package was uploaded by someone on mentors over the weekend and is
already distributed.  It's also now passing all tests on [Debian
CI](https://ci.debian.net/packages/s/siconos/).

## Notes (20/3/2020)

I was just checking to see if siconos had been migrated to testing
yet.  I found it listed as migrated by searching the
debian-testing-changes mailing list
[here](https://lists.debian.org/debian-testing-changes/2020/02/msg00031.html).
So, I went to check buildd to see if it was building in testing, and
discovered it was broken in unstable!  In fact there have been two
bugs filed against the package that I was unaware of, I still don't
know how to ensure I get notified of bugs filed against my package
which is annoying.

The first bug is that the package `swig3.0` has changed to `swig4.0`,
and it says I should build against `swig` in any case.  The other is
some problem during autopkgtest, where apparently it's trying to run a
compiler with the builddir in the path, no idea why, so I'm now trying
to reproduce both bugs.

Meanwhile trying to compile after updating Docker, I get errors from
compiler options that were never a problem before, presumably after a
gcc update.  Now CMake cannot find "Python".

Just further checking the [package tracker
page](https://tracker.debian.org/pkg/siconos), there are two
"dependency" errors and apparently the package is "marked for
autoremoval" from testing!  It would be great to get this news
earlier, but I can't see where to sign up for bug reports.  I _am_ the
maintainer on the package page so I don't understand why I'm not
getting bug reports.  I see a "subscribe" button on the package
tracker, so I am making an account there.  If that doesn't work, I
will ask on IRC.

Now I've added keras and other packages to my tracker subscriptions,
and I see that even keras stopped building and is marked for
autoremoval in 3 days!  Hopefully I can get a quick upload, how
frustrating that I didn't see this earlier.

## Notes (21/3/2020)

Got a response from Daniel Stender today saying he is out of the
packaging and sponsoring business completely, so I will file an RFS
and email the Debian Science list.

## Notes (22/3/2020)

Okay I ended up just sending a request to the Debian Science mailing
list, but haven't had a reply, so now I have uploaded to mentors and
am filing an RFS bug.  However, too lazy to figure out reportbug and
gmail's SMTP yet again, so I copied & pasted the RFS template from
mentors (custom for the package!) and emailed submit@bugs.debian.org
manually using gmail's text-only mode, let's see what happens.

## Notes (23/3/2020)

No replies yet, but I noticed on the package tracker page, the
autoremoval date for keras has been pushed back to April 7.  I don't
know whether this was automatic due to me putting an update on
mentors, or done by someone manually.

## Notes (24/3/2020)

Last night I basically finished with the remaining bugs in the siconos
package, but didn't upload it yet as I need to build it signed.  Also,
I started looking into whether I could fix the problems with test
tolerances on other architectures, basically all builds other than
amd64 are failing due to test tolerances.  I started learning about
how to set up a test environment using qemu after asking on Debian
Science the best way to test this.  After a bit of searching the most
promising instructions are to use qemu "transparently" using the
package qemu-user-static, combined with qemu-debootstrap that allows
you to set up a chroot under a different architecture.  Simplest and
best instructions were found
[here](https://wiki.ubuntu.com/ARM/BuildEABIChroot).  A more extensive
solution for using schroot with multiple architectures is outlined
[here](https://wiki.debian.org/EmDebian/CrossDebootstrap) but I
haven't gotten that far yet.  I started trying the qemu-debootstrap
route but ran out of disk space.  So for now I will upload the fixes.

Anyways today I got an email from Vincent saying that they will be
preparing a new Siconos release, so the architecture fixes can wait
for that update.  I won't wait for the new release to fix the
outstanding bugs, in an attempt to avoid autoremoval.  I will upload
to mentors as soon as the current build is done, since all lintian
errors other than known ones (spelling errors and ABI symbols files)
are now taken care of.

## Notes (25/3/2020)

Looks like siconos got [removed from
testing](https://tracker.debian.org/news/1111935/siconos-removed-from-testing/),
while waiting for a sponsor since yesterday.

## Notes (26/3/2020)

Someone looked at the siconos package and found that autopkgtest
didn't work.  So it's important that I really have the same testing
environment as other packagers, obvious but I've found it difficult to
do so. I re-investigated sbuild and, man, it just worked this time..

Honestly I just followed instructions from the [sbuild
docs](https://wiki.debian.org/sbuild) and things worked fine.

Within docker though it seems important to use a tarball instead of
trying to use an overlay filesystem:

    sudo sbuild-createchroot --include=eatmydata,ccache,gnupg --make-sbuild-tarball=/srv/chroot/unstable-amd64-sbuild.tar.gz  unstable $(mktemp -d) http://130.89.149.21/debian

Then, autopkgtest just worked,

    autopkgtest *.dsc *.deb -- schroot unstable-amd64-sbuild

and I even followed instructions to configure sbuild to run
autopkgtest automatically when building the package: just
make a `~/.sbuildrc` with the following:

    $run_autopkgtest = 1;
    $autopkgtest_root_args = '';
    $autopkgtest_opts = [ '--', 'schroot', '%r-%a-sbuild' ];

Done!

    gbp buildpackage --git-builder=sbuild -A -v -s -d unstable

Adding the previously used `--force-sign` and gpg options to
`buildpackage` did not work as before, so it generates an unsigned
package, but `debsign` can simply be used after the build instead:

    debsign *.dsc *.changes

I was able to upload the resulting signed files to mentors without
issue.

## Notes (28/03/2020)

My upload to mentors was not working.  I received an email from
someone asking me what was up with the package, which was nice.  I
just realized the failure email I received outputs the following
message,

> Make sure you include the full source (if you are using sbuild make
> sure to use the --source option or the equivalent configuration
> item; if you are using dpkg-buildpackage directly use the default
> flags or -S for a source only upload)

so it seems I forgot a flag to sbuild.  Uploaded it again and it
worked.  (Added to instructions in above entry.)

Also, I think I probably wrote just `dput` before, instead of `dput
mentors`, which apparently will just silently fail because it uploads
to the wrong server.

## Notes (05/04/2020)

Seems siconos was finally uploaded today.  Also got a response finally
from my keras packages, only for someone to tell me they must be
'unstable' and not 'UNRELEASED', no thanks to advice from IRC, I
guess.  Package to be autoremoved in 2 days, so I'll try again.

Currently cannot get sbuild to work as detailed above.  I keep getting
errors about missing dh-python, but then discovered it seems to be
operating locally (in the Docker system) instead of in the schroot,
it's very confusing.

Okay, a couple of things.  I forgot to put a temporary directory in
the `sbuild-createchroot` command, so it took the server's http
address as the directory for building the chroot.  Added `$(mktemp
-d)` to the above command.

Secondly, I have to add the user to the `sbuild` group using,

    newgrp sbuild

Thirdly, it seems it _is_ indeed necessary to install `dh-python` and
`python3-setuptools` onto the system that is launching sbuild.

Now building and autopkgtesting keras packages in sbuild, I am indeed
finding a few minor problems that probably would have blocked the
upload.

## Notes (14/04/2020)

As mentioned the siconos package was uploaded to unstable, but when I
checked a couple of days later it was blocked from migration to
testing due to bug #954497, which was strange since this was a bug
closed by my update.  I figure I may have got the syntax in changelog
wrong, because it was the second bug closed on the same line.  I tried
to ask on the mentors mailing list, but instead of a reply, the bug
was closed and the package migrated to testing.  Looking at the [bug
report](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=954497), I
see a comment,

>  The upload didn't trigger closage because of a missing comma.

Which is weird, because I had checked and the policy document [clearly
says](https://www.debian.org/doc/debian-policy/ch-controlfields.html#s-f-closes)

> 5.6.22. Closes
>
> A space-separated list of bug report numbers that the upload
> governed by the .changes file closes.

Anyways I asked in a reply if it's an error in the policy manual.

In the meantime it seems someone (Anton Gladky) finally checked the
keras package update and reported that it failed to test correctly
with 14 failed tests.  I believe he did not include the updated
keras-preprocessing and keras-appications packages while testing,
however I take fault because I realize I forgot to update the required
package versions in control.  I am now testing with this update, I got
it to fail to build with the old packages so he shouldn't get as far
as he did without the updated packages.

Also, in looking into it, I discovered that `sbuild` has an option

    --extra-package=<filename>.deb

which is really handy for this situation of simultaneous updates.

Looks like he has uploaded it and all bugs are closed!

## Notes (19/04/2020)

Working on a "team" is sometimes surprising.  I previously noticed
that lasagne also had some threat of removal from testing on it, but
checking it out it seemed to be due to dependence on a documentation
(javascript) package that was orphaned, but someone had taken
ownership so I figured it would go away.  Decided to check in on it
today, that problem seems gone but now there are testing errors during
CI (I'll have to check my email to see if I was notified?) so I
figured I may as well take a look at it finally.

I started getting it working and had problems with python2 finally
realizing that indeed python-theano has been removed, so okay, no
choice but to remove the python-lasagne package and keep only
python3-lasagne.  So I did it and tried pushing to the master, but
found someone had updated the branch so I checked the results and lo
and behold someone already updated the package and removed
python-lasagne.  Well, no problem, but I just didn't expect so much
autonomous editing of things I'm supposedly responsible for without
any discussion with me.  That's fine...

So I'll see if I can at least update the package and fix the testing
issues.

## Notes (30/04/2020)

A bug #912533 I ignored after exhaustion from the last work on
lasagne, has been upgrade to a "problem" with a new bug #959137,
because it is apparently blocking migration of numpy.  It's really
something that should be fixed upstream, but now that's it has
increased in severity I will patch it and submit a PR, because I think
it's a fairly easy fix.

I just realized I actually never finished with the Lasagne update, so
doing it now.  Done, and emailed Mo Zhou (lumin) since he uploaded the
last updates to the package.

## Notes (12/05/2020)

The package was finally uploaded by Anton Gladky after Mo Zhou said he
could not look at it.  This was done several days ago.  Now I am
confused as a bug has been filed,
[#959647](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=959647),
saying that there are failures to build in sid, however, despite the
bug being filed against the current version, the build logs appear to
be building the previous version.  I will respond to the bug report
asking why.

First, I will try "subscribing" to the bug, since I only received
notice about this due to the package tracker (thank goodness for
that).

## Notes (23/05/2020)

Back onto updating Siconos, as there is a new upstream version 4.3.0
ready to be packaged, and I already did most of the work.  The main
problems I ran into after the release were that fclib needed releasing
and the package updating, and also there were some failures in
autopkgtest because the CMake "targets" that get installed insists on
checking _all_ component libraries even if they aren't needed, so for
example kernel-dev was failing, complaining about about not finding
the control library, which is not even in the COMPONENTS list.

Anyways, I pushed a new master for fclib, and have a working package
for Siconos with a hack in debian/rules that modifies that target
check to only verify libraries in the COMPONENTS list, which isn't
perfectly actualy since it might skip their dependencies but it should
always work nonetheless because of the apt package dependencies so I'm
not too worried about it.

Moving on, I need to clean up the git history ready for pushing to
salsa but I thought I might also take this opportunity to try getting
the package working on more architectures while I'm at it, since I
suspect it's mostly a matter of adjusting the error threshold for
numerical tests.

I'm still researching the best method of setting up a debootstrap
environment with qemu for running ARM etc., [this is the best
one](https://www.hellion.org.uk/blog/posts/foreign-chroots-with-schroot-and-qemu/)
I've found so far, but it's still a multi-step process and I have to
see if it will work with the tarball method I am using inside Docker.
A preliminary test with running debootstrap manually did work,
however.  Another bit of info, similar procedure, [found here for
Raspian](https://gist.github.com/oznu/b5efd7784e5a820ec3746820f2183dc0).

First command:

    $ mkdir /tmp/tmp
    $ sudo debootstrap --variant=buildd --foreign --include=devscripts \
        --arch=armel unstable /tmp/tmp http://httpredir.debian.org/debian
    $ sudo cp /usr/bin/qemu-arm-static /tmp/tmp/usr/bin/
    $ sudo chroot /tmp/tmp
    I have no name!@035d759ffae4:/# ./debootstrap/debootstrap --second-stage

Emitted one warning.  But I continue:

    I have no name!@035d759ffae4:/# exit
    exit

    $ sudo sbuild-createchroot --foreign --setup-only --include=devscripts --arch=armel --make-sbuild-tarball=/srv/chroot/unstable-armel-sbuild.tar.gz unstable /tmp/tmp http://httpredir.debian.org/debian

Again got a warning:

    W: The selected architecture and the current architecture do not match
    I: You probably need to add a personality option (see schroot(1))

But let's see.. it says:

    I: Successfully set up unstable chroot.

However the tarball is missing:

    $ ls /var/lib/sbuild/unstable-arm.tar.gz
    ls: cannot access '/var/lib/sbuild/unstable-arm.tar.gz': No such file or directory

Okay I had some success by creating it manually:

    $ cd /tmp/tmp
    $ sudo tar -czf /srv/chroot/unstable-armel-sbuild.tar.gz .
    $ cp /srv/chroot/unstable-armel-sbuild.tar.gz ~/

Last step is a backup that came in handy.
Now I could shell in and finish the process from that blog post:

    $ sudo sbuild-shell unstable-armel-sbuild
    I: /bin/sh
    # rm /usr/bin/qemu-arm-static
    # exit

Ah, but then after that I cannot shell in again:

    $ sudo sbuild-shell unstable-armel-sbuild
    E: Failed to create group sbuild
    Failed to set up chroot
    Error setting up unstable-armel-sbuild chroot
    Chroot setup failed at /usr/bin/sbuild-shell line 42.

After restoring the tarball from a backup, I tried:

    $ sudo sbuild-shell unstable-armel-sbuild
    I: /bin/sh
    # apt-get install qemu-user-static

Didn't like that either.  :sad:
I'll try to ignore this step and just build the package...

This is the full command I've been using, note addition of `--arch`
and the name of the schroot:

    $ [ $PWD == /home/sinclairs/projects/debian/siconos ] && \
      git reset --hard && git clean -xfd && \
      gbp buildpackage --git-ignore-branch --git-builder=sbuild\
        -A -v -d unstable-armel-sbuild --arch=armel --source\
        --extra-package=../libfclib0_3.1.0+dfsg-1_amd64.deb\
        --extra-package=../libfclib-dev_3.1.0+dfsg-1_amd64.deb\
        2>&1 | tee ../log

It failed, but the failure has to do with a bad dependency between
`libgcc1` and `gcc-10-base`, so I *think* it's just a run-of-the-mill
temporary problem in unstable, I will try again later.  Confirmed,
even my usual amd64 schroot is giving the same issue.

This brings up another issue I should make sure to tackle before
submitting the package for upload, ensure it compiles with gcc-10,
otherwise I'll end up having to fix it later, and I'd rather it stays
"good" as long as possible.

Reading up a bit, it seems armel is an older architecture, and armhf
supports floating point, so probably that should be my target for
Siconos, but let's see how many architectures we can comfortably test
on once this is working.

Okay, I did `sbuild-shell` into the armel chroot and did `apt-get
install -f` and that seems to have fixed the problem.  Package now
building!  I have to handle fclib first, however.  Ah, also seems
maybe there is a problem with openblas on armel.  I will try with
armhf.

Yes, after setting up armhf similar to above, `libopenblas-dev`
package is available in armhf and not in armel.

Hm, unfortunately fclib did not build although it seemed to be going
fine at first, but CMake failed on an early step while checking the C
compiler (`CMakeCompilerIdDetection.cmake`), so that's unfortunate.
I should at least try to make sure that the i386 build is okay!

After building an i386 sbuild similarly to above, the following
command worked for fclib, autopkgtest successful!

    [ $PWD == /home/sinclairs/projects/debian/fclib ] && \
    git reset --hard && git clean -xfd && \
    gbp buildpackage --git-ignore-branch --git-builder=sbuild\
      -A -v -d unstable --arch=i386 --source 2>&1 | tee ../log

Turns out you do _not_ need to specify the arch in the schroot name
for the `-d` option here (failed with `unstable-i386-sbuild`, worked
with just `unstable`).

As for siconos, it gets as far as compiling `kernel.i` and then fails
with an out of memory error, which completely makes sense as this
stage takes about 6 GB of memory and would not fit in a 32-bit
addressable area; had not thought of that.  It means 32-bit versions
of siconos can probably only be built using cross-compilation, which
presents another challenge.  Possibly disabling serialization might
work.

Following [building with
dpkg-buildpackage](https://wiki.debian.org/CrossCompiling#Building_with_dpkg-buildpackage)
to create a cross-compilation environment in the Docker container.

    $ sudo dpkg --add-architecture i386
    $ sudo apt-get update
    $ sudo apt-get install crossbuild-essential-i386

Unfortunately I could not install `fclib` packages due to conflicts
with the already-installed version!

    $ dpkg -i ../libfclib*i386*.deb

After removing the installed `libfclib0`, the above worked, but also
needed a couple of iterations of `apt-get install -f`.

Finally, no, can't get i386 system set up in the Docker unstable
container.  Continuing to try building for i386 without serialization.
Maybe I'll try again using a chroot as that seems like the right way.

Just discovered how to get a shell inside the schroot when the build
fails.  Put this in `~/.sbuildrc`:

    $external_commands = {
        "build-failed-commands" => [ [ '%SBUILD_SHELL' ] ],
    };

See `/etc/sbuild/sbuild.conf` for more info and other useful triggers.


## Notes (06/06/2020)

Giving up on the architecture stuff, but I got the siconos package
updated on salsa after reorganizing the commits, and now looking
at 3 bugs that have been filed in the meanwhile.

  * https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=962219
  * https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=961735
  * https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=957794

The last two require me to install packages from experimental, so I am
now trying to figure out how to do that.  So far I made a backup copy
of my schroot for unstable and used `sbuild-shell` to go in and add
apt sources for experimental, [as described
here](https://aerostitch.github.io/linux_and_unix/debian/sbuild_with_experimental_distribution.html)

    $ sudo sbuild-shell unstable
    # echo "deb http://ftp.debian.org/debian experimental main" > /etc/apt/sources.list.d/experimental.list
    # echo "deb-src http://ftp.debian.org/debian experimental main" >> /etc/apt/sources.list.d/experimental.list
    # echo "Package: *\nPin: release a=unstable\nPin-Priority:900" > /etc/apt/preferences.d/unstable.pref
    # echo "Package: *\nPin: release a=experimental\nPin-Priority:800" > /etc/apt/preferences.d/experimental.pref
    # exit
    # sudo sbuild-update -udcar unstable

I am having a hard time reproducing the problem with mumps (#961735)
because the new version is not failing for me and as far as I can tell
the version in *unstable* is 5.3.1, despite the bug claiming that I
can install it from experimental.  Maybe it made the transition
already.  Yes, following up the transition bug it says that the
"transition is all done (apart from siconos)", so I guess that's the
right package and the update will fix it.

I also can't reproduce the problem with boost (#962219) which only
complains about autopkgtest anyways which are now fixed, so this
should also be fine.  So just to test with GCC-10 (#957794).

Not sure if there's a way to get sbuild to do it automatically, so I
used `sbuild-shell` again to install `g++` from experimental:

    # apt-get install g++

Confirms that GCC 9.2 is installed (already installed).

    # apt-get -t experimental install g++

Installs the new GCC-10 package.  Now I try building the package as
normal using sbuild.  A bit scared because the build errors provided
in the bug report are to do with Fortran.

Did not work, due to some apt problem.

    The following packages have unmet dependencies:
     sbuild-build-depends-main-dummy : Depends: gfortran but it is not going to be installed
                                       Depends: libfclib-dev (>= 3.1.0) but 3.0.0+dfsg-2+b1 is to be installed
    E: Unable to correct problems, you have held broken packages.
    apt-get failed.
    E: Package installation failed
    
I'll try installing `gfortran` by hand in `sbuild-shell`, also from
experimental.  No cigar, but it is complaining about the outdated
fclib, even though I specified the deb with `--extra-packages`, so
hopefully if I solve that... I'll try [installing it into the schroot
manually](https://wiki.ubuntu.com/SimpleSbuild).

Okay, I managed to install fclib by exposing a directory duing
`sbuid-shell` via `fstab` and manually doing `dpkg -i`, which also
required install its dependencies using apt-get.  Now siconos seems to
be building!  At least, getting through dependency installs.

And now it does indeed reproduce the errors in that issue!  Time to
work on them, but I'll do so by installing GCC 10 in the Docker image
directly.

## Notes (08/06/2020)

Last night I finished the fclib and siconos packages and uploaded them
to mentors.  Someone actually looked at it already today to my
surprise.  However, after fclib was accepted and uploaded, it seems it
is mysteriously segfaulting during one of the tests on buildd.  The
uploader seems equally confused, saying he tested it several times on
his own machine without encountering that issue.  So, I have to try to
reproduce it.

I just realized what it could be, I believe that I built the packages
after testing for the GCC-10 bug, and therefore built them against an
sbuild that had gcc-10 installed, and therefore several other packages
from experimental.  I will try to see if it reproduces the issue.

## Notes (18/11/2020)

I have updated siconos a couple of times since the last entry, it took
a while to find uploaders sometimes (Anton Gladky did it) but
otherwise nothing significant, just some bug fixes.

There is a new bug reported on recent h5py, currently testing to see
if I can reproduce.  I am still dedicated to keeping the package in
testing until the freeze.  There is a new upstream version so I might
work on that after seeing to this bug.

I submitted to a WNPP bug to orphan `lasagna` since I could not easily
solve its bug with the mocking framework, and in any case theano is
EOL so effectively Lasagna is too.

Keras is currently not reporting any bugs for quite some time,
thankfully.

## Notes (19/11/2020)

Orphaning bug for Lasagne was received and on the debian tracker page
says it is orphaned and needing a new maintainer.  I am still listed
as the uploader for the previous version; on the packages.debian.org
page I am still listed as a maintainer along with Debian Science.

Investigating h5py but, it is really only when testing against h5py in
experimental.  Found that there is also a failure on `boost-defaults`
in experimental, [this is where to
check](https://ci.debian.net/packages/s/siconos/unstable/amd64/).

However, I am having trouble building the package for experimental
because `python3-vtk7` depends on Python < 3.9, and autopkgtest fails
due to some error about python3-siconos also not being compatible with
Python 3.9 (fair enough, it's compiled only for the default Python;
that's something I didn't write here, but one of the bugs I handled
was that it was not picking the right Python, and when I asked how to
tell it which Python, the only answer I got was that I should build
for all Pythons, which I don't know how to do.  Shrug.)

Anyways, failed to get temporary pinned packages for experimental in
sbuild, but in the unstable Docker I simply installed the experimental
version of h5py by following
[instructions](https://wiki.debian.org/DebianExperimental]).
Compiling now, and we'll see if I can just run the test manually.

Update, I was able to run the test with no errors after installing the
built packages in Docker, so, so far unable to reproduce.

## Notes (12/12/2020)

The other day I uploaded a fix to the two bugs filed against siconos
for packages in experimental.  Well, one was filed as a bug, the other
was just seen on buildd when I was checking into the other one.  As
noted above, took a while to reproduce.  In fact I could only do it
by installing in the Docker, unfortunately didn't take complete notes
on that.

Update: command history suggests that I finally copied and edited the
command found in the debian CI log.  E.g., to test with experimental
version of "boost-defaults", I used:

    /usr/bin/autopkgtest -s --no-built-binaries --apt-upgrade '--add-apt-source=deb http://incoming.debian.org/debian-buildd buildd-experimental main contrib non-free' --add-apt-release=experimental --pin-packages=experimental=src:boost-defaults  siconos -- schroot unstable-amd64-sbuild

Anyways, I put the upload on mentors and selected that it needs a
sponsor and it got uploaded without further interaction, which was
nice.

After that, I updated the package to 4.3.1 and it got through to
passing autopkgtest without issues.  I installed it and did some
manual testing and it seemed fine, and since it's a point release I
considered not incrementing the soname, so in the end I just pushed to
mentors.  Unfortunately on mentors it noted some lintian errors,
notably I forgot to change UNRELEASED to 'unstable' again.  But also I
forgot that I should check the symbols files.

So I deleted it from mentors and still have a bit of work to do I
guess.

In the meantime, I noticed lately that the Debian package tracker page
now points to Ubuntu, and indeed the siconos package has been picked
up by Ubuntu, which is great news.  I was able to test is for the
in-progress Ubuntu release for next year by running it via Docker, and
it's indeed installable and usable from that image.  Fantastic.


## Notes (13/12/2020)

Updating siconos package to new version, steps:

1. gbp import-orig --uscan
2. dch -v 4.3.1+dfsg-1
3. gbp pq rebase (etc.. fix/update patches)
4. gbp buildpackage --git-builder=sbuild -A -v -s -d unstable 2>&1 | tee ../log
5. update debian policy, dh version
6. lintian -i -I -E --pedantic --show-overrides siconos_4.3.1+dfsg-1_amd64.changes
7. gbp buildpackage (outside sbuild)
8. dpkg-gensymbols -plibsiconos-numerics6 -Odebian/libsiconos-numerics6.symbols -q # and check results
9. dch -r
10. gbp buildpackage --git-builder=sbuild -A -v -s -d unstable 2>&1 | tee ../log
11. debsign siconos_4.3.1+dfsg-1_amd64.changes
12. dput mentors

Lintian error: I tried to change architecture for "siconos" binary
package from "any" to "all", to fix "?", but this just resulted in
"not-binnmuable-all-depends-any".  What's the right thing to do?  As
the message suggests, now trying to change "binary:Version" to
"source:Version".

Looking into another lintian pedantic warning about missing upstream
metadata.  Apparently yet another place to put info about upstream.
I take note that the docs recommend using a web tool that I didn't know
about for searching packages: http://codesearch.debian.net

## Notes (14/12/2020)

I tried setting dep to `source:Version` as suggested by lintian but this still resulted in
`not-binnmuable-all-depends-any`.  Okay, instructions actually say to use `>=` and not `=`
so I will try that.

## Notes (26/01/2021)

Debian either has entered freeze or is close to it for the next release and I'm really
trying to ensure that siconos gets included.

My update for siconos 4.3.1 had been sitting on mentors for a while, since I had been
hoping it would just get noticed like last time instead of having to beg for a sponsor
again, but then I noticed (after not checking my email in weeks) that Anton Gladky had put
out a request for packages looking for last-minute updates before the freeze, so I
answered about the siconos update.

He decided that now was a good time to be more vigilent.  He enabled the salsa pipeline,
which failed, and the failure was hard for me to explain since it seemed only related to
uploading the artifacts, otherwise it was successful.  (Previously it could not build
due to log file limits, which I why I disabled it.)

So, I mentioned this, and that the i386 build was not expected to work.  He fixed those
problems, nice of him, by disabling debug symbols for salsa, which reduced the artifact
size, and disabling the i386 build.  He then uploaded the package.

I got the usual errors from buildd, and did not worry.  However, he then emailed again and
asked if I could fix or disable the failing test for arm64.  Well, this was actually just
ContactTest, which timed out, and I know that's too long a test anyway, so I agreed to
disable it.  I have since done so, but I responded to say that I am not sure if the
package will work on arm64.  Actually I'm surprised it got that far, but I think maybe the
errors I got previously were more with autopkgtest?  I don't recall -- I worked on it
a bit over Christmas on my Raspberry Pi but didn't successfully solve some bugs related
to ARM64, so I left it.  In any case, I am now uncertain of the package status, since it
seems that due to the upload it was removed from testing, but due to the failure on ARM,
it is blocked from being migrated from unstable to testing.  This is very disappointing,
so I guess we will see what happens when I get a response about disabling ContactTest.

In the meantime, I am trying to get an ARM64 Debian environment set up on my computer
under qemu.  It has been extremely difficult to get an image to build, but finally I had
some success using the prebuilt OpenStack image.  I have resized the image file so that
there's more room and I will try building on it, I have to set up an entire devscripts
environment and sbuild image, etc., and it all runs pretty slowly on emulation, so we'll
see how it goes.  But at least I can give it more memory than the Raspberry Pi has.  I
don't think I'll get it working tonight; honestly I don't have much time for this since I
have work to do for Actronika, but now I'm risking losing the last 3 years of work on this
package.. if it doesn't get included in the release I'll be really frustrated.

## Notes (30/01/2021)

Since last post, I decided to bite the bullet and try building the ARM64 package in qemu
so that I could do it without worrying about memory or disk space.  It was harder than
expected to set up a running qemu debian image from scratch.  I followed instructions and
tried two versions of the installer but it kept getting stuck looking for the install
media.  Finally I figured out how to adapt the OpenStack image to a larger disk size,
which I should record here.

Anyways, once I got a working Debian system running under qemu, I was able to install all
the devscripts, sbuild and git-buildpackage et al, create a schroot, and build the
package.  It took almost 2 full days to build!  Which was a lot of time using my computer
with the fan running hot.  Fortunately I had recently during Christmas upgraded my
computer to 24 GB of RAM and 2 TB of harddrive space so I didn't have to worry about
memory or space exhaustion.  Unfortunately, the tests did time out, so I had to build it
again after disabling the timeout.  But, then it actually built the package!  Moreover, it
passed autopkgtest.  So, I emailed Anton Gladky with this information and today it seems
he has uploaded the new version and it apparently passed tests on buildd for all platforms
that support its dependencies, except i386.  Fantastic.

To get the Debian image working, I first [downloaded a pre-built qcow2 image from
OpenStack](https://cdimage.debian.org/cdimage/openstack/current/).  Following their
[docs](https://docs.openstack.org/image-guide/modify-images.html), I was able to then
resize the disk to 50 GB using the `virt-resize` tool from
[libguestfs](https://www.libguestfs.org/).  I also had to follow the docs to mount the
disk using [qemu-nbd](https://www.mankier.com/8/qemu-nbd), and install my ssh keys as
authorized so that I could log in.  I also had to use `chroot` on the mounted disk to set
a password for the `debian` user, before I was able to log in.

Prior to this I had tried making an image myself, first simply using standard qemu and an
install media (`mini.iso`) from the latest Debian release, and then using a
[script](https://github.com/emojifreak/qemu-arm-image-builder) linked from the [debian
wiki](https://wiki.debian.org/Arm64Qemu) but it just didn't work no matter what I tried.
However, the prebuilt image was perfect, and I could ssh into it without problems, leave a
screen session running etc.  The only real downside was the extreme slowness of it.

## Notes (07/02/2021)

Since last entry, arm64 version has been uploaded and did compile successfully on buildd.
Received an email that siconos 4.3.1-2 has been migrated to testing.  Quite happy about
this.

## Notes (26/10/2021)

I have been updating the siconos package to 4.4.0.  After struggling with a number of
oversights in the release, including the fact that they didn't release a new version of
fclib despite requiring some functions from git master (currently submitted a PR about
that), I am finally at the stage of looking through lintian output.

One of the warnings that it outputs is that the patches have not been submitted upstream,
which in several cases is true, but in other cases I do not intend to submit them.  I
noticed that the lintian message mentions something called
[DEP-8](https://dep-team.pages.debian.net/deps/dep3/), so I looked that up, and apparently
there are some standards for the headers of patches that I could use to tell lintian about
my intentions.

So I need to rewrite the patch headers to follow that, I guess.

Apart from that there are some things related to the symbols files, due to a change to the
soname.  (Which was not included in the release but Vincent applied a commit immediately
after the release to change it, so I included that as a patch.)

Also something about "io" not including fortified libc, which is odd, since I do have
those options turned on.

## Notes (16/09/2022)

Updated Siconos and Keras packages.  Have not been taking regular notes here, as no new
knowledge or tools were really needed, but updating Siconos has actually taken about 6
months.  That is, I tried to update it at the beginning of the year and didn't quite
finish the job.  I updated the patches, and found all kinds of problems, and got blocked
by needing changes to `fclib` but no release being done, and I didn't end up reaching out
to the Siconos team about that.

Finally, I had some time again, and this time around I decided that maintaining the
connection to `fclib` probably just isn't worth it, or I could temporarily disconnect
them, and so I just went ahead and did that.  So I still don't know what the right
procedure is for updating two packages simultaneously.

In any case, there were a lot of problems to iron out, beyond just updating the patches
and symbols, and once I finally got the Siconos package working, it still failed in
testing with an actual problem in one of the numerical routines.  I did have to bring that
up with the Siconos team once I'd isolated it, and they provided a timely fix.  I included
that as a new patch, and then I still had to update a lot of the autopkgtest.  so overall
it took me several full days of work, stretched over like 2 week.  Now I got everything
passing, *include* reprotest, which I'll describe, and Now the package is waiting on
mentors.  I will send out some RFS emails if they don't get uploaded soon.

The only thing that doesn't pass actually is the ARM64 build.  However, I tried making a
Docker ARM environment, which worked well, and in that environment I was actually able to
build the ARM version, although I didn't carry the build all the way through, but on Salsa
it fails at package install, which it got past.  So I'll leave it at that.

In any case, once I uploaded that package, I worked on Keras for a bit, found and fixed
some problems and then figured out which open bugs they were associated with. So the
update closes 3 bugs.  I already argued on the mailing list that this package should not
bother to be updated, because the software is EOL, but since I had some time I just went
for it.  However, it cannot be updated to the latest version, because after the current
version, it only supports TensorFlow.  So for now, this should be fine, until a TensorFlow
package is available, then I'll see if it includes the built-in Keras and I'll try to
agree with people that the Keras package is superseded.

## Notes (23/09/2022)

By now both new packages, Siconos and Keras, have been uploaded by someone, without
prompting.  I guess people do actually look at the mentors uploads periodically.

So, all good.  But, today someone, actually the same uploader, filed a bug against
Siconos, which is clearly a failing test, but only on a weird esoteric CPU architecture
that I'm unfamiliar with, called `s390x`, where an assertion is triggered in the
`NM_test`.  Now I'll have learn something about that architecture I guess.

Anyway, it's worth noting, because this is also an aspect of Debian that has come up for
me in the past, where a bug was filed against Liblo because its memory layout caused
crashes on SPARC if I remember correctly, and my answer at the time was basically that it
would just cause too many changes and annoyances to change everything just for SPARC so
I'd rather consider SPARC unsupported, because I can guarantee that there are a total of
zero people using liblo on SPARC.

At the time, the response was that, _well, in Debian we try to support all these
architectures, so I prefer to fix it_, followed by a huge, invasive patch that I refused
to apply.  Finally I asked the guy if he was fixing this because he _needed_ it or because
it was somehow out of "principle", and of course it was the latter, so I just left it at
that.  But, as upstream, I had the freedom to do so.  As a packager, I guess I'm in the
other position, of trying to support this weird platform.  But I'm a bit at a loss as to
how to do so, as I don't have access to an `s390x` computer, and I can't debug something
that I can't reproduce.  So yeah.  Not sure what to do.  I suppose I'll see if I can use a
similar Docker-qemu trick with this architecture, and if so I can see if it's
reproducible, but overall this is kind of annoying because similar to Liblo, I can
guarantee that a total of zero people need to run Siconos on `s390x`.  But, I'll make a
small effort, and then let's see what to do, I might have to reply to the bug and ask what
the hell to even do about this.

I should first look up what `s390x` even is.

Also: reading a thread by someone asking about their own package uploads for Debian
Science, I see a similar situation that I had with `fclib`, where two packages need to be
updated simultaneously.  There, they suggestion reading
[this](https://wiki.debian.org/Teams/ReleaseTeam/Transitions) and
[this](https://release.debian.org/transitions/index.html).  Overall looks like more
trouble than it's worth, involving uploading to `experimental` and requesting a
"transition slot" by `reportbug`.  Ugh.  Okay, but at least it's possible I guess, but it
would take quite a bit longer than just a normal upload.

The discussion also complains a lot about upstream and how they package their work with a
compiled Makefile etc., and how they should submit bugs to upstream to fix things and have
little confidence that upstream will ever "do things right".  It's really weird to me how
Debian always thinks they can impose on upstream like that.  I mean, for very serious
problems where things don't build or whatever, sure, but why is it not fine to just
include a fix in `patches` or `rules` and be done with it, it's really not a big deal but
they really get all worked up about these things.  Which I find annoying for some reason.

## Notes (10/01/2023)

Noticed some emails related to `keras` package recently.  It turned out to be something
*really* low-level and picky in the way it uses some Python types to handle loading old
pickle files, causing problems with Python 3.11.  I decided I had enough of trying to keep
this package up to date, given that upstream no longer supports it.  So, I finally went
ahead and figured out how to orphan the three `keras` packages.  Actually now Google
allows creating specific passwords for apps that need to use SMTP instead of requiring you
to go in and temporarily set your account to "low security mode", which is much better
because now I can permanently configure `reportbug`.  I filed a big against `wnpp` and it
just guided me through it.

Then I discovered that there were also some bug reports for Siconos, so I have been slowly
fixing those, needed to make about 5 patches but all pretty simple things.  So I'm almost
ready to build the final one, sign it, and upload to mentors.

## Notes (28/01/2023)

Finally got a new version of the Siconos package uploaded.  One thing that delayed me was
that I spent too much time trying to figure out why the `piuparts` test on Salsa was
failing.  It turns out it was also testing something regarding the previous version of the
packages.  Since the previous version apparently has some dependency contraint on old
versions of packages that are no longer in Debian Unstable, it was failing to install
them. (Duh?)  To be honest I'm not even sure where these contraints came from, e.g. it was
inventing a constraint of "Python < 3.10", which isn't written anywhere in my control
file.

Anyway, after a couple of posts to the mentors mailing list I finally decided to just
ignore this and go ahead and upload it.  I took it down a couple of times because I had
some lintian error with the watch file and then I realized I forgot to close the bug in
the changelog.  After the third upload, the package got uploaded by someone without any
extra prompting from me, which was nice.
